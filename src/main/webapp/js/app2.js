function bindEscapeKey() {

    var KEYCODE_ESC = 27;

    $(document).keyup(function(e) {
        if (e.keyCode == KEYCODE_ESC) {
            $('.close').click();
        }
    });
}

function submitShopItem() {
    document.forms["submitShopItemForm"].submit();
}
