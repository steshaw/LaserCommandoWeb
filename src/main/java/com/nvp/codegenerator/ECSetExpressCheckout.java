/*
 * Copyright 2005, 2008 PayPal, Inc. All Rights Reserved.
 *
 * SetExpressCheckout NVP example; last modified 08MAY23. 
 *
 * Initiate an Express Checkout transaction.  
 */
package com.nvp.codegenerator;

import au.com.lasercommando.model.product.ProductOrder;
import com.paypal.sdk.core.nvp.NVPDecoder;
import com.paypal.sdk.core.nvp.NVPEncoder;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.NVPCallerServices;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * PayPal Java SDK sample code
 */
public class ECSetExpressCheckout {

    private static Logger logger = Logger.getLogger(ECSetExpressCheckout.class);
    private NVPCallerServices caller = null;

    public NVPDecoder ECSetExpressCheckoutCode(String returnURL, String cancelURL, String totalDiscount,
            String totalAmount, String paymentType, String currencyCode, String username, String password,
            String signature, String environment, String itemPrice, String orderDescription, String quantity,
            String version, List<ProductOrder> productOrderList) {

        NVPEncoder encoder = new NVPEncoder();
        NVPDecoder decoder = new NVPDecoder();

        try {
            caller = new NVPCallerServices();
            APIProfile profile = ProfileFactory.createSignatureAPIProfile();
            /*
             * WARNING: Do not embed plaintext credentials in your application
             * code. Doing so is insecure and against best practices. Your API
             * credentials must be handled securely. Please consider encrypting
             * them for use in any production environment, and ensure that only
             * authorized individuals may view or modify them.
             */

            // Set up your API credentials, PayPal end point, API operation and version.
            profile.setAPIUsername(username);
            profile.setAPIPassword(password);
            profile.setSignature(signature);
            profile.setEnvironment(environment);

            caller.setAPIProfile(profile);
            encoder.add("VERSION", version);
            encoder.add("METHOD", "SetExpressCheckout");

            // Add request-specific fields to the request string.
            encoder.add("RETURNURL", returnURL);
            encoder.add("CANCELURL", cancelURL);

            //order
            encoder.add("L_PAYMENTREQUEST_0_NAME0", orderDescription);
            encoder.add("L_PAYMENTREQUEST_0_QTY0", quantity);
            encoder.add("L_PAYMENTREQUEST_0_AMT0", itemPrice);

            //discount
            int counter = 1;
            if (!totalDiscount.equals("-0.00")) {
                encoder.add("L_PAYMENTREQUEST_0_NAME" + counter, "Player discount");
                encoder.add("L_PAYMENTREQUEST_0_QTY" + counter, "1");
                encoder.add("L_PAYMENTREQUEST_0_AMT" + counter, totalDiscount);
                counter++;
            }

            //extras
            for (ProductOrder productOrder : productOrderList) {
                encoder.add("L_PAYMENTREQUEST_0_NAME" + counter, productOrder.getProduct().getName());
                encoder.add("L_PAYMENTREQUEST_0_QTY" + counter, String.valueOf(productOrder.getQuantity()));
                encoder.add("L_PAYMENTREQUEST_0_AMT" + counter, productOrder.getProduct().getPrice().toString());
                counter++;
            }

            encoder.add("PAYMENTREQUEST_0_AMT", totalAmount);
            encoder.add("PAYMENTREQUEST_0_CURRENCYCODE", currencyCode);

            encoder.add("PAYMENTACTION", paymentType);
//            encoder.add("PAYPALADJUSTMENT",totalDiscount);

            // Execute the API operation and obtain the response.
            String NVPRequest = encoder.encode();
            String NVPResponse = caller.call(NVPRequest);
            decoder.decode(NVPResponse);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return decoder;
    }
}
