/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model;

/**
 *
 * @author Kobus
 */
public enum TeamEnum {
    RED(1, "Red"),
    BLUE(2, "Blue");
    
    private int code;
    private String description;

    private TeamEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }    

    public static TeamEnum findEnum(String description) {
        for (TeamEnum enumValue : values()) {
            if (enumValue.getDescription().equals(description)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + description);
    }
    
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
        
}
