/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.product;

/**
 *
 * @author KobusVM
 */
public enum ProductTypeEnum {
    CLOTHING(1, "Clothing"),
    PARTY_HIRE(2, "Party hire"),
    TOYS(3, "Toys"),
    VOUCHERS(4, "Vouchers"),
    BOOKING(5, "Booking");
    
    private int code;
    private String description;

    private ProductTypeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

        
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
}
