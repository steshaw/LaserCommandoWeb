/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.product;

/**
 *
 * @author KobusVM
 */
public enum ProductPriceTypeEnum {
    RENT(1, "Rent"),
    SELL(2, "Sell");
    
    private int code;
    private String description;

    private ProductPriceTypeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

        
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
}
