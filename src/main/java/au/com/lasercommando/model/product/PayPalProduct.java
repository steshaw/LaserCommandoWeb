/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.product;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author dutoitk
 */
@Document
public class PayPalProduct implements java.io.Serializable {
    @Id
    private String id;
    private String sandboxHostedButtonId;
    private String hostedButtonId;
    private Product product;    
    private int sortOrder;
    private String optionListName;    
    private List<PayPalProductOption> optionList;
    private boolean active;

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    public boolean isOptionsEmpty() {
        return optionList.isEmpty();
    }

    public List<PayPalProductOption> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<PayPalProductOption> optionList) {
        this.optionList = optionList;
    }

    public String getOptionListName() {
        return optionListName;
    }

    public void setOptionListName(String optionListName) {
        this.optionListName = optionListName;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getHostedButtonId() {
        return hostedButtonId;
    }

    public void setHostedButtonId(String hostedButtonId) {
        this.hostedButtonId = hostedButtonId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSandboxHostedButtonId() {
        return sandboxHostedButtonId;
    }

    public void setSandboxHostedButtonId(String sandboxHostedButtonId) {
        this.sandboxHostedButtonId = sandboxHostedButtonId;
    }

    public PayPalProduct() {
        id = java.util.UUID.randomUUID().toString();
        optionList = new ArrayList<PayPalProductOption>();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PayPalProduct other = (PayPalProduct) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.sandboxHostedButtonId == null) ? (other.sandboxHostedButtonId != null) : !this.sandboxHostedButtonId.equals(other.sandboxHostedButtonId)) {
            return false;
        }
        if ((this.hostedButtonId == null) ? (other.hostedButtonId != null) : !this.hostedButtonId.equals(other.hostedButtonId)) {
            return false;
        }
        if (this.product != other.product && (this.product == null || !this.product.equals(other.product))) {
            return false;
        }
        if (this.sortOrder != other.sortOrder) {
            return false;
        }
        if ((this.optionListName == null) ? (other.optionListName != null) : !this.optionListName.equals(other.optionListName)) {
            return false;
        }
        if (this.optionList != other.optionList && (this.optionList == null || !this.optionList.equals(other.optionList))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 17 * hash + (this.sandboxHostedButtonId != null ? this.sandboxHostedButtonId.hashCode() : 0);
        hash = 17 * hash + (this.hostedButtonId != null ? this.hostedButtonId.hashCode() : 0);
        hash = 17 * hash + (this.product != null ? this.product.hashCode() : 0);
        hash = 17 * hash + this.sortOrder;
        hash = 17 * hash + (this.optionListName != null ? this.optionListName.hashCode() : 0);
        hash = 17 * hash + (this.optionList != null ? this.optionList.hashCode() : 0);
        return hash;
    }    
}