/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model;

/**
 *
 * @author KobusVM
 */
public enum WeaponEnum {
    COBRA(1, "Cobra", "tn_cobraright10001.jpg", "http://www.youtube.com/v/Qx8MDrAi6vA", 1.8, 40),
    SCORPION(2, "Scorpion sub-machine gun", "tn_scorpion350.jpg", "http://www.youtube.com/v/BY1BJLNQZD8", 2, 38),
    COMMANDO_CARBINE(3, "Commando Carbine", "tn_commando.jpg", "http://www.youtube.com/v/kXRXrn50p5w", 3.5, 53),
    MORITA_SNIPER(4, "Morita Sniper Rifle", "tn_moritasniper350.jpg", "http://www.youtube.com/v/-LaVLa84CME", 4.3, 88);
    
    private int code;
    private String name;
    private String image;
    private String video;
    private double weight;
    private int length;

    private WeaponEnum(int code, String name, String image, String video, double weight, int length) {
        this.code = code;
        this.name = name;
        this.image = image;
        this.video = video;
        this.weight = weight;
        this.length = length;
    }    

    public static WeaponEnum findEnum(String name) {
        for (WeaponEnum enumValue : values()) {
            if (enumValue.getName().equals(name)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for name " + name);
    }
    
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }        
}
