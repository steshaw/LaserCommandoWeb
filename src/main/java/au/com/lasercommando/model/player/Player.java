/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.player;

import au.com.lasercommando.model.WeaponEnum;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;

/**
 *
 * @author KobusVM
 */
public class Player implements java.io.Serializable {
    @Id
    private String id;
    private String avatar;
    private String name;
    private String surname;
    private int age;
    private boolean receiveCorrespondence;
    private WeaponEnum preferWeapon;
    private String email;
    private String telephone;
    private Clan clan;
    private String gender;
    private String callSign;
    private int rank;
    private int gamesPlayed;
    private List<Achievement> achievementList;
    private List<String> gameCodeList;
    private List<String> achievementCodeList;

    public boolean isReceiveCorrespondence() {
        return receiveCorrespondence;
    }

    public void setReceiveCorrespondence(boolean receiveCorrespondence) {
        this.receiveCorrespondence = receiveCorrespondence;
    }
    
    public List<String> getAchievementCodeList() {
        return achievementCodeList;
    }

    public void setAchievementCodeList(List<String> achievementCodeList) {
        this.achievementCodeList = achievementCodeList;
    }
    
    public List<String> getGameCodeList() {
        return gameCodeList;
    }

    public void setGameCodeList(List<String> gameCodeList) {
        this.gameCodeList = gameCodeList;
    }
    
    public void addGameCode(String gameCode) {
        gameCodeList.add(gameCode);
    }
    
    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public List<Achievement> getAchievementList() {
        return achievementList;
    }

    public void setAchievementList(List<Achievement> achievementList) {
        this.achievementList = achievementList;
    }

    public void addAchievement(Achievement achievement) {
        achievementList.add(achievement);
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getCallSign() {
        return callSign;
    }

    public void setCallSign(String callSign) {
        this.callSign = callSign;
    }
    
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }        

    public WeaponEnum getPreferWeapon() {
        return preferWeapon;
    }

    public void setPreferWeapon(WeaponEnum preferWeapon) {
        this.preferWeapon = preferWeapon;
    }
    
    public Clan getClan() {
        return clan;
    }

    public void setClan(Clan clan) {
        this.clan = clan;
    }
    
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
        
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Player() {
        id = java.util.UUID.randomUUID().toString();
        rank = RankEnum.UNRANKED.getCode();
        receiveCorrespondence = true;
        achievementList = new ArrayList<Achievement>();
        gameCodeList = new ArrayList<String>();
        achievementCodeList = new ArrayList<String>();
    }
}
