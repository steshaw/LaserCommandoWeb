/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.player;

import javax.persistence.Id;

/**
 *
 * @author dutoitk
 */
public class Achievement implements java.io.Serializable {
    @Id
    private String id;
    private int achievementCode;
    private String name;
    private String description;
    private String url;
    private int numberRequired;
    private int numberDone;

    public int getAchievementCode() {
        return achievementCode;
    }

    public void setAchievementCode(int achievementCode) {
        this.achievementCode = achievementCode;
    }

    public int getNumberDone() {
        return numberDone;
    }

    public void setNumberDone(int numberDone) {
        this.numberDone = numberDone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberRequired() {
        return numberRequired;
    }

    public void setNumberRequired(int numberRequired) {
        this.numberRequired = numberRequired;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Achievement() {
        id = java.util.UUID.randomUUID().toString();
    }
}
