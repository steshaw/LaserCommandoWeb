/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.player;

import au.com.lasercommando.util.ShortIdGenerator;
import javax.persistence.Id;

/**
 *
 * @author dutoitk
 */
public class GenAchievement implements java.io.Serializable {
    @Id
    private String id;
    private int achievementCode;
    private int totalAvailable;
    private int totalUsed;
    private String alternateId;

    public int getAchievementCode() {
        return achievementCode;
    }

    public void setAchievementCode(int achievementCode) {
        this.achievementCode = achievementCode;
    }

    public String getAlternateId() {
        return alternateId;
    }

    public void setAlternateId(String alternateId) {
        this.alternateId = alternateId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTotalAvailable() {
        return totalAvailable;
    }

    public void setTotalAvailable(int totalAvailable) {
        this.totalAvailable = totalAvailable;
    }

    public int getTotalUsed() {
        return totalUsed;
    }

    public void setTotalUsed(int totalUsed) {
        this.totalUsed = totalUsed;
    }

    public GenAchievement() {
        id = java.util.UUID.randomUUID().toString();
        alternateId = ShortIdGenerator.generate();
        totalAvailable = 1;
    }
}
