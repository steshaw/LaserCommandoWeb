/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.player;

/**
 *
 * @author Kobus
 */
public enum RankEnum {
    UNRANKED(0, "Maggot", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/MAGGOT_fin.jpg", 10),
    PRIVATE(1, "Private", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/PRIVATE_fin.jpg", 20),
    PRIVATE_G2(2, "Private(Grade 2)", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 30),
    CORPORAL(3, "Corporal", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 40),
    CORPORAL_G2(4, "Corporal(Grade 2)", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 50),
    SERGEANT(5, "Seargeant", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 60),
    SERGEANT_G2(5, "Sergeant(Grade 2)", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 70),
    SERGEANT_G3(5, "Sergeant(Grade 3)", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 80),
    GUNNERY(5, "Gunnery sergeant", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 90),
    GUNNERY_G2(5, "Gunnery sergeant(Grade 2)", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 100),
    GUNNERY_G3(5, "Gunnery sergeant(Grade 3)", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 110),
    MASTER_SERGEANT(5, "Master sergeant", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 110),
    LIEUTENANT(5, "Lieutenant", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 120),
    CAPTAIN(5, "Captain", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 999999),
    MAJOR(5, "Major", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 999999),
    COLONEL(5, "Colonel", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 999999),
    BRIGADIER(5, "Brigadier", "https://s3-ap-southeast-1.amazonaws.com/lasercommandoimages/ranks/private.png", 999999);
    
    private int code;
    private String description;
    private String url;
    private int gamesRequiredToNextLevel;

    private RankEnum(int code, String description, String url, int gamesRequiredToNextLevel) {
        this.code = code;
        this.description = description;
        this.url = url;
        this.gamesRequiredToNextLevel = gamesRequiredToNextLevel;
    }    

    public static RankEnum findEnum(String roleDescription) {
        for (RankEnum enumValue : values()) {
            if (enumValue.getDescription().equals(roleDescription)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + roleDescription);
    }

    public static RankEnum findEnum(int code) {
        for (RankEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }

    public int getGamesRequiredToNextLevel() {
        return gamesRequiredToNextLevel;
    }

    public void setGamesRequiredToNextLevel(int gamesRequiredToNextLevel) {
        this.gamesRequiredToNextLevel = gamesRequiredToNextLevel;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
        
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
        
}
