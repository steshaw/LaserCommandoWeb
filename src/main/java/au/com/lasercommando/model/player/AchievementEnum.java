/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.player;

/**
 *
 * @author Kobus
 */
public enum AchievementEnum {
    PARTY_ANIMAL(1, "Party animal", "Played a game on your birthday", "/images/ranks/private.png", 1, false),
    TERMINATOR(2, "Terminator", "Played as the Terminator", "/images/ranks/private.png", 5, false),
    ARMOURER(3, "Armourer", "Owns their own gun", "/images/ranks/private.png", 1, false),
    HOT_WEATHER(4, "Hot weather service", "Played in 40 degrees heat", "/images/ranks/private.png", 5, false),
    FOUL_WEATHER(5, "Foul weather service", "Played while it was storming", "/images/ranks/private.png", 5, false),
    COLD_WEATHER(5, "Cold weather service", "Played in 10 degrees weather", "/images/ranks/private.png", 5, false),
    SQUAD_LEADER(6, "Squad leader", "Led a squad", "/images/ranks/private.png", 5, false),
    MEDIC(7, "Medical service", "Been a medic", "/images/ranks/private.png", 5, false),
    SCENARIO_DEV(8, "Scenario development", "Developed their own scenario that has been used", "/images/ranks/private.png", 1, false),
    NIGHT_TIME(9, "Night time service", "Played missions at night", "/images/ranks/private.png", 5, false),
    GAME_CREATOR(10, "Game creator", "Organised games", "/images/ranks/private.png", 5, false),
    PUBLIC_GAMER(11, "Public gamer", "Played public games", "/images/ranks/private.png", 5, false),
    PRIVATE_GAMER(12, "Private gamer", "Played private games", "/images/ranks/private.png", 5, false),
    RENTAL_GAMER(13, "Rental gamer", "Rented packages", "/images/ranks/private.png", 5, false),
    EXPLOSIVE_ORDNANCE(14, "Explosive ordnance", "Used a claymore mine in missions", "/images/ranks/private.png", 5, false),
    RESCUER(15, "Rescuer", "Did a course in emergency response", "/images/ranks/private.png", 1, false),
    MAD_HATTER(16, "Mad hatter", "Bought one of each hat", "/images/ranks/private.png", 2, false),
    RADIO_GAMER(17, "Radio gamer", "Played missions using walkie talkies", "/images/ranks/private.png", 5, false),
    VIP(18, "VIP", "Be the VIP in VIP missions", "/images/ranks/private.png", 5, false),
    THE_KID(19, "Billy the Kid", "Play missions with the 6 shooter", "/images/ranks/private.png", 5, false),
    FOREST_WARFARE(21, "Forest warfare", "Won clan wars in the forest", "/images/ranks/private.png", 5, false),
    URBAN_WARFARE(22, "Urban warfare", "Won clan war at an urban field", "/images/ranks/private.png", 5, false),
    CLAN_WAR_PARTICIPANT(23, "Clan war participant", "Has participated in clan wars", "/images/ranks/private.png", 5, false),
    DUAL_WIELD(24, "Dual wield", "Played using two weapons at the same time", "/images/ranks/private.png", 5, false);
    
    private int code;
    private String name;
    private String description;
    private String url;
    private int numberRequired;
    private boolean active;

    private AchievementEnum(int code, String name, String description, String url, int numberRequired, boolean active) {
        this.code = code;
        this.name = name;
        this.description = description;
        this.url = url;
        this.numberRequired = numberRequired;
    }    

    public static AchievementEnum findEnum(String description) {
        for (AchievementEnum enumValue : values()) {
            if (enumValue.getDescription().equals(description)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + description);
    }

    public static AchievementEnum findEnum(int code) {
        for (AchievementEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }

    public int getNumberRequired() {
        return numberRequired;
    }

    public void setNumberRequired(int numberRequired) {
        this.numberRequired = numberRequired;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
        
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }    
}
