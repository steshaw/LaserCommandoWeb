/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.player;

/**
 *
 * @author dutoitk
 */
public class PlayerProxy {
    private Player player;
    private int calculatedRankProgress;
    private String currentRankImageUrl;
    private String currentRankDescription;
    private String nextRankImageUrl;
    private String nextRankDescription;

    public String getCurrentRankDescription() {
        return currentRankDescription;
    }

    public String getNextRankDescription() {
        return nextRankDescription;
    }

    public String getNextRankImageUrl() {
        return nextRankImageUrl;
    }

    public int getCalculatedRankProgress() {
        return calculatedRankProgress;
    }

    public String getCurrentRankImageUrl() {
        return currentRankImageUrl;
    }

    public Player getPlayer() {
        return player;
    }

    public PlayerProxy(Player player) {
        this.player = player;
        this.calculatedRankProgress = calculateProgress();
        RankEnum rankEnum = RankEnum.findEnum(player.getRank());
        RankEnum nextRankEnum = RankEnum.findEnum(rankEnum.getCode() + 1);
        
        currentRankImageUrl = rankEnum.getUrl();
        currentRankDescription = rankEnum.getDescription();

        nextRankImageUrl = nextRankEnum.getUrl();
        nextRankDescription = nextRankEnum.getDescription();
    }

    private int calculateProgress() {
        int previousGamesRequired = 0;

        if (player.getRank() > 0) {
            RankEnum previousRankEnum = RankEnum.findEnum(player.getRank() - 1);
            previousGamesRequired = previousRankEnum.getGamesRequiredToNextLevel();
        }
        RankEnum rankEnum = RankEnum.findEnum(player.getRank());
        double gamesDelta = player.getGamesPlayed() - previousGamesRequired;
        double gamesRequiredToLevel = rankEnum.getGamesRequiredToNextLevel() - previousGamesRequired;
        double calculatedProgress = gamesDelta / gamesRequiredToLevel * 100;
        return new Double(calculatedProgress).intValue();
    }
}
