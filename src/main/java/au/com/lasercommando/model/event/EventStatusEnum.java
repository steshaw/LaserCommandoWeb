/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

/**
 *
 * @author Kobus
 */
public enum EventStatusEnum {
    BOOKED(1, "Booked"),
    AVAILABLE(2, "Available"),
    CLOSED(3, "Closed");
    
    private int code;
    private String description;

    private EventStatusEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

        
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
    

    public static EventStatusEnum findEnum(String description) {
        for (EventStatusEnum enumValue : values()) {
            if (enumValue.getDescription().equals(description)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + description);
    }    

    public static EventStatusEnum findEnum(int code) {
        for (EventStatusEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }    
}
