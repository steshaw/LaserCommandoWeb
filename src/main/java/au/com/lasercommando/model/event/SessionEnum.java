/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

/**
 *
 * @author Kobus
 */
public enum SessionEnum {
    SESSION1(1, "09:00 - 11:00", 9),
    SESSION2(2, "11:30 - 13:30", 11),
    SESSION3(3, "14:00 - 16:00", 14),
    SESSION4(4, "19:00 - 21:00", 19);
    
    private int code;
    private String description;
    private int startHour;

    private SessionEnum(int code, String description, int startHour) {
        this.code = code;
        this.description = description;
        this.startHour = startHour;
    }    

    public static SessionEnum findEnum(String description) {
        for (SessionEnum enumValue : values()) {
            if (enumValue.getDescription().equals(description)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + description);
    }

    public static SessionEnum findEnum(int code) {
        for (SessionEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }
}
