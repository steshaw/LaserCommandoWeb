/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

import java.util.HashMap;

/**
 *
 * @author dutoitk
 */
public class ContactHelper {
    private static final HashMap<Integer, String> averageAgeHash = new HashMap<Integer, String>();
    static {
        averageAgeHash.put(AverageAgeEnum.GROUP1.getCode(), AverageAgeEnum.GROUP1.getDescription());
        averageAgeHash.put(AverageAgeEnum.GROUP2.getCode(), AverageAgeEnum.GROUP2.getDescription());
        averageAgeHash.put(AverageAgeEnum.GROUP3.getCode(), AverageAgeEnum.GROUP3.getDescription());
        averageAgeHash.put(AverageAgeEnum.GROUP4.getCode(), AverageAgeEnum.GROUP4.getDescription());
        averageAgeHash.put(AverageAgeEnum.GROUP5.getCode(), AverageAgeEnum.GROUP5.getDescription());
        averageAgeHash.put(AverageAgeEnum.GROUP6.getCode(), AverageAgeEnum.GROUP6.getDescription());
        averageAgeHash.put(AverageAgeEnum.GROUP7.getCode(), AverageAgeEnum.GROUP7.getDescription());
    }

    public static String convertAverageAgeGroup(int averageAgeGroup) {
        return averageAgeHash.get(averageAgeGroup);
    }
}
