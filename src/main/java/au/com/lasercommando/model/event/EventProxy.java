/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

import au.com.lasercommando.Constants;
import au.com.lasercommando.model.map.AuthorEnum;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KobusVM
 */
public class EventProxy implements java.io.Serializable {

    private Event event;
    private String sessionDescription;
    private String statusDescription;
    private String authorDescription;
    private String dateFormatted;
    private List<Payment> paidPaymentList;

    public List<Payment> getPaidPaymentList() {
        return paidPaymentList;
    }

    public void setPaidPaymentList(List<Payment> paidPaymentList) {
        this.paidPaymentList = paidPaymentList;
    }

    public String getDateFormatted() {
        return dateFormatted;
    }
    
    public String getAuthorDescription() {
        return authorDescription;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public String getSessionDescription() {
        return sessionDescription;
    }

    public Event getEvent() {
        return event;
    }


    public String getEventCapacity() {
        int available = event.getMaxPlayerCount() - event.getPlayersJoined();
        if (available > 20) {
            return ">20";
        } else {
            return String.valueOf(available);
        }
    }

    public EventProxy(Event event) {
        if (event == null) return ;
        
        this.event = event;
        
        if (event.getDate() != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
            dateFormatted = dateFormat.format(event.getDate());            
        }

        List<Payment> originalPaymentList = event.getPaymentList();
        paidPaymentList = new ArrayList<Payment>();
        for (Payment payment : originalPaymentList) {
            if (payment.isPaid()) {
                paidPaymentList.add(payment);
            }
        }
        
        if (event.getSession() > 0) {
            sessionDescription = SessionEnum.findEnum(event.getSession()).getDescription();
        }
        if (event.getStatus() > 0) {
            statusDescription = EventStatusEnum.findEnum(event.getStatus()).getDescription();
        }
        if (event.getAuthor() > 0) {
            authorDescription = AuthorEnum.findEnum(event.getAuthor()).getDescription();
        }
    }
}
