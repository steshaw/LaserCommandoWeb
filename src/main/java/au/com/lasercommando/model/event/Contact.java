/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

/**
 *
 * @author KobusVM
 */
public class Contact implements java.io.Serializable {
    private String id;
    private String firstname;
    private String surname;
    private String telephoneNumber;
    private String email;
    private String groupName;
    private int averageAgeGroup;
    private boolean eventCreator;
    private String howDidYouFindUs;

    public String getHowDidYouFindUs() {
        return howDidYouFindUs;
    }

    public void setHowDidYouFindUs(String howDidYouFindUs) {
        this.howDidYouFindUs = howDidYouFindUs;
    }

    public int getAverageAgeGroup() {
        return averageAgeGroup;
    }

    public void setAverageAgeGroup(int averageAgeGroup) {
        this.averageAgeGroup = averageAgeGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public boolean isEventCreator() {
        return eventCreator;
    }

    public void setEventCreator(boolean eventCreator) {
        this.eventCreator = eventCreator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getAverageAgeGroupDescription() {
        return ContactHelper.convertAverageAgeGroup(averageAgeGroup);
    }
    
    public Contact() {
        id = java.util.UUID.randomUUID().toString();
    }
}
