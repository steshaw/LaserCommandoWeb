/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

/**
 *
 * @author Kobus
 */
public enum EventTypeEnum {
    PRIVATE(1, "Private"),
    PUBLIC(2, "Public");
    
    private int code;
    private String description;

    private EventTypeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

        
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
    

    public static EventTypeEnum findEnum(String description) {
        for (EventTypeEnum enumValue : values()) {
            if (enumValue.getDescription().equals(description)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + description);
    }    
}
