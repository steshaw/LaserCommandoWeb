/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

import java.util.HashMap;

/**
 *
 * @author dutoitk
 */
public class SessionHelper {
    private static final HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
    static {
        hashMap.put(SessionEnum.SESSION1.getCode(), SessionEnum.SESSION1.getDescription());
        hashMap.put(SessionEnum.SESSION2.getCode(), SessionEnum.SESSION2.getDescription());
        hashMap.put(SessionEnum.SESSION3.getCode(), SessionEnum.SESSION3.getDescription());
        hashMap.put(SessionEnum.SESSION4.getCode(), SessionEnum.SESSION4.getDescription());
    }

    public static String convertSession(int session) {
        return hashMap.get(session);
    }
}
