/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

import com.paypal.sdk.core.nvp.NVPDecoder;

/**
 *
 * @author KobusVM
 */
public class CheckOutResponse {
    private NVPDecoder response;
    
    public CheckOutResponse(NVPDecoder response) {
        this.response = response;
    }
    
    public String getTotalAmount() {
        return response.get("PAYMENTREQUEST_0_AMT");
    }
    
    public String getToken() {
        return response.get("TOKEN");
    }
}
