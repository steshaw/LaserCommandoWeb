/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

/**
 *
 * @author Kobus
 */
public enum AverageAgeEnum {
    GROUP1(1, "5 to 8 years", 5),
    GROUP2(2, "8 to 10 years", 8),
    GROUP3(3, "10 to 12 years", 10),
    GROUP4(4, "12 to 14 years", 12),
    GROUP5(5, "14 to 16 years", 14),
    GROUP6(6, "16 to 18 years", 16),
    GROUP7(7, "18+", 18);
    
    private int code;
    private String description;
    private int startAge;

    private AverageAgeEnum(int code, String description, int startAge) {
        this.code = code;
        this.description = description;
        this.startAge = startAge;
    }    

    public static AverageAgeEnum findEnum(String description) {
        for (AverageAgeEnum enumValue : values()) {
            if (enumValue.getDescription().equals(description)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + description);
    }

    public static AverageAgeEnum findEnum(int code) {
        for (AverageAgeEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStartAge() {
        return startAge;
    }

    public void setStartAge(int startAge) {
        this.startAge = startAge;
    }
}
