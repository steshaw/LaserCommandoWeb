/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

import au.com.lasercommando.model.product.ProductOrder;
import au.com.lasercommando.util.ShortIdGenerator;
import java.util.List;

/**
 *
 * @author dutoitk
 */
public class Payment implements java.io.Serializable {
    private String id;
    private int paidForPlayers;
    private double discountAmount;
    private double totalPlayerCost;
    private double totalCost;
    private Contact contact;
    private boolean paid;
    private double totalExtrasCost;
    private String paymentTokenId;
    private List<ProductOrder> productOrderList;

    public String getPaymentTokenId() {
        return paymentTokenId;
    }

    public void setPaymentTokenId(String paymentTokenId) {
        this.paymentTokenId = paymentTokenId;
    }

    public double getTotalExtrasCost() {
        return totalExtrasCost;
    }

    public void setTotalExtrasCost(double totalExtrasCost) {
        this.totalExtrasCost = totalExtrasCost;
    }
    
    public double getTotalPlayerCost() {
        return totalPlayerCost;
    }

    public void setTotalPlayerCost(double totalPlayerCost) {
        this.totalPlayerCost = totalPlayerCost;
    }
    
    public List<ProductOrder> getProductOrderList() {
        return productOrderList;
    }

    public void setProductOrderList(List<ProductOrder> productOrderList) {
        this.productOrderList = productOrderList;
    }
        
    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }
        
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPaidForPlayers() {
        return paidForPlayers;
    }

    public void setPaidForPlayers(int paidForPlayers) {
        this.paidForPlayers = paidForPlayers;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
    
    public Payment() {
        contact = new Contact();
        id = ShortIdGenerator.generate();
    }
}
