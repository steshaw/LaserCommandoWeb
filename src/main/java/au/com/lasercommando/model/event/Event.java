/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.event;

import au.com.lasercommando.model.player.Player;
import au.com.lasercommando.Constants;
import au.com.lasercommando.model.map.Map;
import au.com.lasercommando.model.map.AuthorEnum;
import au.com.lasercommando.model.map.MapCategoryEnum;
import au.com.lasercommando.model.map.MapDifficultyEnum;
import au.com.lasercommando.util.ShortIdGenerator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author KobusVM
 */
@Document
public class Event implements java.io.Serializable {
    @Id
    private String id;
    private String name;
    private Date date;
    private boolean privateEvent;
    private String mapId;
    private String contactId;
    private Map map;
    private int session;
    private int status;
    private int playersJoined;
    private int maxPlayerCount;
    private String password;
    private List<Player> playerList;
    private List<Payment> paymentList;    
    private int author;
    private double pricePerPlayer;
    private String alternateId;
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }        

    public String getAlternateId() {
        return alternateId;
    }

    public void setAlternateId(String alternateId) {
        this.alternateId = alternateId;
    }

    public double getPricePerPlayer() {
        return pricePerPlayer;
    }

    public void setPricePerPlayer(double pricePerPlayer) {
        this.pricePerPlayer = pricePerPlayer;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }
    
    public boolean isPrivateEvent() {
        return privateEvent;
    }

    public void setPrivateEvent(boolean privateEvent) {
        this.privateEvent = privateEvent;
    }

    public boolean getPrivateEvent() {
        return privateEvent;
    }

    public void addPayment(Payment payment) {
        paymentList.add(payment);
    }

    public List<Payment> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<Payment> paymentList) {
        this.paymentList = paymentList;
    }

    public void addPlayer(Player player) {
        playerList.add(player);
    }

    public Collection<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public int getMaxPlayerCount() {
        return maxPlayerCount;
    }

    public void setMaxPlayerCount(int maxPlayerCount) {
        this.maxPlayerCount = maxPlayerCount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPlayersJoined() {
        return playersJoined;
    }

    public void setPlayersJoined(int playersJoined) {
        this.playersJoined = playersJoined;
    }        

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSession() {
        return session;
    }

    public String getSessionDescription() {
        return SessionHelper.convertSession(session);
    }

    public void setSession(int session) {
        this.session = session;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDateFormatted() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
        return dateFormat.format(date);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }
    
    public Event() {
        id = java.util.UUID.randomUUID().toString();
        playerList = new ArrayList<Player>();
        paymentList = new ArrayList<Payment>();
        alternateId = ShortIdGenerator.generate();
        active = true;
        
        privateEvent = true;
        status = EventStatusEnum.AVAILABLE.getCode();
        maxPlayerCount = 40;
        playersJoined = 10;
        author = AuthorEnum.CUSTOM.getCode();
        
        map = new Map();
        map.setMapCategory(MapCategoryEnum.INFLATABLES.getCode());
        map.setMapDifficulty(MapDifficultyEnum.LEVEL_TWO.getCode());
    }
}
