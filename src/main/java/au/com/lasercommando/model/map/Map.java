/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.map;

import au.com.lasercommando.model.event.Contact;
import java.util.List;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Kobus
 */
@Document
public class Map implements java.io.Serializable  {
    @Id
    private String id;
    private String name;
    private String description;
    private int mapCategory;
    private int mapDifficulty;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String coords;
    private String website;
    private int zoom;    
    private List<Contact> contactList;
    private double pricePerPlayer;

    public double getPricePerPlayer() {
        return pricePerPlayer;
    }

    public void setPricePerPlayer(double pricePerPlayer) {
        this.pricePerPlayer = pricePerPlayer;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public String getLocation() {
        StringBuilder location = new StringBuilder();
        location.append(address1).append(", ");
        location.append(address2).append(", ");
        location.append(city).append(", ");
        location.append(state).append(", ");
        location.append(postalCode);
        
        return location.toString();
    }
    
    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }
    
    public String getCoords() {
        return coords;
    }

    public void setCoords(String coords) {
        this.coords = coords;
    }
    
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMapCategory() {
        return mapCategory;
    }

    public void setMapCategory(int mapCategory) {
        this.mapCategory = mapCategory;
    }

    public String getMapCategoryDescription() {
        return MapCategoryEnum.findEnum(mapCategory).getDescription();
    }
    
    public int getMapDifficulty() {
        return mapDifficulty;
    }

    public void setMapDifficulty(int mapDifficulty) {
        this.mapDifficulty = mapDifficulty;
    }

    public String getMapDifficultyDescription() {
        return MapDifficultyEnum.findEnum(mapDifficulty).getDescription();
    }
    
    public Map() {
        id = java.util.UUID.randomUUID().toString();
        zoom = 18;
        state = "QLD";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Map other = (Map) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }     
}
