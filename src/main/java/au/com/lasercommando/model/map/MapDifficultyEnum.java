/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.map;

/**
 *
 * @author Kobus
 */
public enum MapDifficultyEnum {
    LEVEL_ONE(1, "Indoor laser games"), //school hall
    LEVEL_TWO(2, "School oval"), //school oval, music
    LEVEL_THREE(3, "Woodlands camp site with slight growth"), //scouts camp 2
    LEVEL_FOUR(4, "Woodlands camp site with lots of growth"), //scouts camp 9
    LEVEL_FIVE(5, "Woodlands camp site night game"); //scouts night
    
    private int code;
    private String description;

    private MapDifficultyEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

        
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static MapDifficultyEnum findEnum(int code) {
        for (MapDifficultyEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }

}
