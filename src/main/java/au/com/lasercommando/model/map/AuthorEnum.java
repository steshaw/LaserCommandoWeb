/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.map;

/**
 *
 * @author Kobus
 */
public enum AuthorEnum {
    LASER_COMMANDO(1, "Laser Commando Maps"),
    CUSTOM(2, "Custom Map");
    
    private int code;
    private String description;

    private AuthorEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }    

    public static AuthorEnum findEnum(String roleDescription) {
        for (AuthorEnum enumValue : values()) {
            if (enumValue.getDescription().equals(roleDescription)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + roleDescription);
    }

    public static AuthorEnum findEnum(int code) {
        for (AuthorEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }
    
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
        
}
