/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model.map;

/**
 *
 * @author Kobus
 */
public enum MapCategoryEnum {
    UNSPECIFIED(0, "Unspecified"),
    WOODLANDS(1, "Woodlands"),
    INFLATABLES(2, "Inflatables"),
    URBAN(3, "Urban");
    
    private int code;
    private String description;

    private MapCategoryEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }    

    public static MapCategoryEnum findEnum(String roleDescription) {
        for (MapCategoryEnum enumValue : values()) {
            if (enumValue.getDescription().equals(roleDescription)) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for description " + roleDescription);
    }

    public static MapCategoryEnum findEnum(int code) {
        for (MapCategoryEnum enumValue : values()) {
            if (enumValue.getCode() == code) {
                return enumValue;
            }
        }

        throw new RuntimeException("Enum not found for code " + code);
    }
    
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
        
}
