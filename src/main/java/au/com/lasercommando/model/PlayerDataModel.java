/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model;

import au.com.lasercommando.model.player.Player;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author KobusVM
 */
public class PlayerDataModel extends ListDataModel<Player> implements SelectableDataModel<Player>, java.io.Serializable {    
  
    public PlayerDataModel() {  
    }  
  
    public PlayerDataModel(List<Player> data) {  
        super(data);  
    }  
      
    @Override  
    public Player getRowData(String rowKey) {  
        List<Player> players = (List<Player>) getWrappedData();  
          
        for(Player player : players) {  
            if(player.getId().equals(rowKey))  
                return player;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Player player) {  
        return player.getId();  
    }  
}  
                      