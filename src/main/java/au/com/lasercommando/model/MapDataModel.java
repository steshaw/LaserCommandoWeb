/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.model;

import au.com.lasercommando.model.map.Map;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author KobusVM
 */
public class MapDataModel extends ListDataModel<Map> implements SelectableDataModel<Map>, java.io.Serializable {    
  
    public MapDataModel() {  
    }  
  
    public MapDataModel(List<Map> data) {  
        super(data);  
    }  
      
    @Override  
    public Map getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<Map> maps = (List<Map>) getWrappedData();  
          
        for(Map map : maps) {  
            if(map.getId().equals(rowKey))  
                return map;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Map map) {  
        return map.getId();  
    }  
}  
                      