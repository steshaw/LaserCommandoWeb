/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package au.com.lasercommando.model.gallery;

public class Album implements java.io.Serializable {
    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Album(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public Album() {
    }
}
