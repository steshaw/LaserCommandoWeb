/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import org.apache.log4j.Logger;

/**
 *
 * @author KobusVM
 */
public class FacebookAccessToken {

    private static Logger logger = Logger.getLogger(FacebookAccessToken.class);

    public static synchronized String retrieveAccessToken(final String facebookAccessTokenUrl) {
        String token = "";
        try {
            StringBuilder tokenBuilder = new StringBuilder();
            URL htmlPage = new URL(facebookAccessTokenUrl);
            BufferedReader in = new BufferedReader(new InputStreamReader(htmlPage.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                tokenBuilder.append(inputLine);
            }
            in.close();

            token = tokenBuilder.toString();
            String[] stringSplit = token.split("=");
            token = stringSplit[1];

        } catch (Exception ex) {
            logger.error("Reading access token failed for url " + facebookAccessTokenUrl, ex);
        }
        return token;
    }
}
