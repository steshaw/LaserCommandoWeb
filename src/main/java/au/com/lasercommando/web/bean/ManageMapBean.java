/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.map.Map;
import au.com.lasercommando.model.map.MapCategoryEnum;
import au.com.lasercommando.service.MapService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author KobusVM
 */
@Controller
@Scope("session")
public class ManageMapBean implements java.io.Serializable {
    @Resource
    private MapService mapService;
    private Map selectedMap;
    private boolean editMap;

    public MapCategoryEnum[] getMapCategoryList() {
        return MapCategoryEnum.values();
    }

    public boolean isEditMap() {
        return editMap;
    }

    public void setEditMap(boolean editMap) {
        this.editMap = editMap;
    }

    public Map getSelectedMap() {
        return selectedMap;
    }

    public void setSelectedMap(Map selectedMap) {
        this.selectedMap = selectedMap;
    }
    
    public List<Map> getAllMaps() {
        return mapService.findAll();
    }

    @PostConstruct
    public void init() {
        editMap = false;
        selectedMap = new Map();
    }

    public String edit(Map map) {
        this.selectedMap = map;
        editMap = true;
        
        return "managemap.jsf?faces-redirect=true";
    }

    public String update() {
        mapService.save(selectedMap);
        editMap = false;
        return "maplist.jsf?faces-redirect=true";
    }

    public String add() {
        mapService.save(selectedMap);
        return "maplist.jsf?faces-redirect=true";
    }

}
