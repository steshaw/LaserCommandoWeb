/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

/**
 *
 * @author dutoitk
 */
@Controller
@Scope("view")
public class PricesBean implements java.io.Serializable {
    @Value("${discount.10.players}")
    private double discount10Players;
    @Value("${discount.20.players}")
    private double discount20Players;
    @Value("${discount.30.players}")
    private double discount30Players;
    @Value("${discount.40.players}")
    private double discount40Players;

    public double getDiscount10Players() {
        return discount10Players * 100;
    }

    public double getDiscount20Players() {
        return discount20Players * 100;
    }

    public double getDiscount30Players() {
        return discount30Players * 100;
    }

    public double getDiscount40Players() {
        return discount40Players * 100;
    }
}
