/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.Constants;
import au.com.lasercommando.model.event.*;
import au.com.lasercommando.model.map.AuthorEnum;
import au.com.lasercommando.model.map.Map;
import au.com.lasercommando.model.product.PayPalProduct;
import au.com.lasercommando.model.product.Product;
import au.com.lasercommando.service.EventService;
import au.com.lasercommando.service.MapService;
import au.com.lasercommando.service.PayPalProductService;
import au.com.lasercommando.service.PlayerService;
import au.com.lasercommando.service.ProductService;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author KobusVM
 */
@Controller
@Scope("session")
public class ManageEventBean implements java.io.Serializable {

    @Resource
    private EventService eventService;
    @Resource
    private MapService mapService;
    @Resource
    private MessageSource messageSource;
    @Value("${price.per.player}")
    private double pricePerPlayer;
    private List<Map> mapList;
    private List<EventProxy> eventList;
    private EventProxy selectedEvent;
    private boolean editEvent;
    @Resource
    private PayPalProductService payPalProductService;
    @Resource
    private ProductService productService;

    public boolean isEditEvent() {
        return editEvent;
    }

    public void setEditEvent(boolean editEvent) {
        this.editEvent = editEvent;
    }

    public List<Map> getMapList() {
        return mapList;
    }

    public void setMapList(List<Map> mapList) {
        this.mapList = mapList;
    }

    public AuthorEnum[] getAuthorList() {
        return AuthorEnum.values();
    }

    public EventStatusEnum[] getEventStatusList() {
        return EventStatusEnum.values();
    }

    public SessionEnum[] getSessionList() {
        return SessionEnum.values();
    }

    public EventProxy getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(EventProxy selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public List<EventProxy> getAllEvents() {
        return eventList;
    }

    @PostConstruct
    public void init() {
        editEvent = false;
        selectedEvent = new EventProxy(new Event());
    }

    public String loadAlternateEventIdList() {
        eventList = eventService.findActiveEvents();
        return "event/eventlistDisplayAlternateId.jsf?faces-redirect=true";
    }

    public String loadEditEventList() {
        eventList = eventService.findActiveEvents();
        return "event/eventlist.jsf?faces-redirect=true";
    }

    public String newEvent() {
        initEvent();
        return "event/manageevent.jsf?faces-redirect=true";
    }

    private void initEvent() {
        this.selectedEvent = new EventProxy(new Event());
        selectedEvent.getEvent().setPlayersJoined(0);
        selectedEvent.getEvent().setMaxPlayerCount(Constants.MAX_PLAYERS);
        selectedEvent.getEvent().setStatus(EventStatusEnum.BOOKED.getCode());
        selectedEvent.getEvent().setPrivateEvent(false);
        selectedEvent.getEvent().setAuthor(AuthorEnum.LASER_COMMANDO.getCode());
        selectedEvent.getEvent().setPricePerPlayer(pricePerPlayer);
        mapList = mapService.findAll();
    }

    public String displayAlternateId(EventProxy eventProxy) {
        this.selectedEvent = eventProxy;
        return "eventAlternateId.jsf?faces-redirect=true";
    }

    public String edit(EventProxy eventProxy) {
        this.selectedEvent = eventProxy;
        editEvent = true;
        mapList = mapService.findAll();

        return "manageevent.jsf?faces-redirect=true";
    }

    public String update() {
        eventService.save(selectedEvent.getEvent());
        editEvent = false;
        return "eventlist.jsf?faces-redirect=true";
    }

    public void add() {
        eventService.save(selectedEvent.getEvent());
        initEvent();
        String newEvent = messageSource.getMessage("new.event.created", new Object[]{}, Locale.US);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, newEvent, newEvent);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void specialChangeEventPricing() {
        List<EventProxy> eventList = eventService.findAll();
        for (EventProxy eventProxy : eventList) {
            Event event = eventProxy.getEvent();
            event.setPricePerPlayer(25);
            eventService.save(event);
        }
    }

    public void specialPopulateProducts() {
        List<PayPalProduct> list = payPalProductService.findAllActive();
        for (PayPalProduct payPalProduct : list) {
            Product product = payPalProduct.getProduct();
            product.setId(java.util.UUID.randomUUID().toString());
            product.setSortOrder(payPalProduct.getSortOrder());
            productService.save(product);
        }
    }

    public void specialChangeMapName() {
        List<EventProxy> eventProxyList = eventService.findAll();
        for (EventProxy eventProxy : eventProxyList) {
            Event event = eventProxy.getEvent();
            Map map = event.getMap();
            if ((map != null) && (map.getName() != null)) {
                if (map.getName().equals("Zillmere State School Oval")) {
                    map.setDescription("Zillmere State School Oval has some trees on the one side.  We will try to place the inflatables in a way that the trees also become part of the game.  That should help with getting some shade.  BBQ's are allowed.  There isn't any electricity, but we do have generators that can be hired.  We have access to the toilets of the school.  There is ample area for parking in Merritt Avenue adjacent to the oval.");
                    eventService.save(event);
                } else if (map.getName().equals("Murrenbong Scouts Camp 2")) {
                    map.setDescription("Murrongong Scouts Camp 2 has a porter potty, we can order a second one if needed.  There is a covered area about the size of a single garage.  The scouts do allow BBQ's, please contact them to make sure if they will allow your type of BBQ.  There isn't any electricity, but we do have generators that can be hired.");
                    eventService.save(event);
                }
            }
        }
    }

}
