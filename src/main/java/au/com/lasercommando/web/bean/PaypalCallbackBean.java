/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.event.Event;
import au.com.lasercommando.model.event.EventProxy;
import au.com.lasercommando.model.event.EventStatusEnum;
import au.com.lasercommando.service.EventService;
import javax.annotation.Resource;
import javax.faces.event.ComponentSystemEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author dutoitk
 */
@Controller
@Scope("view")
public class PaypalCallbackBean implements java.io.Serializable {
    @Resource
    private EventService eventService;
    @Resource
    private EventSelectionBean eventSelectionBean;
    private Event bookedEvent;

    public Event getBookedEvent() {
        return bookedEvent;
    }

    public void setBookedEvent(Event bookedEvent) {
        this.bookedEvent = bookedEvent;
    }

    public void bookingSuccessful(ComponentSystemEvent event) {
        EventProxy selectedEvent = eventSelectionBean.getSelectedEvent();
        selectedEvent.getEvent().setStatus(EventStatusEnum.BOOKED.getCode());
        eventService.save(selectedEvent.getEvent());
        EventProxy eventProxy = eventService.findEvent(selectedEvent.getEvent().getId());
        if (eventProxy != null) {
            bookedEvent = eventProxy.getEvent();
        }

        eventSelectionBean.init();
    }
}
