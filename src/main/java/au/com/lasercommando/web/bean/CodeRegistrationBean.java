/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.player.Player;
import au.com.lasercommando.model.player.PlayerProxy;
import au.com.lasercommando.service.CodeRegistrationService;
import java.util.Locale;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author dutoitk
 */
@Controller
@Scope("view")
public class CodeRegistrationBean implements java.io.Serializable {
    @Resource
    private FacebookBean facebookBean;
    @Resource
    private CodeRegistrationService codeRegistrationService;
    @Resource
    private MessageSource messageSource;
    private String gameCode;
    private String achievementCode;

    public String getAchievementCode() {
        return achievementCode;
    }

    public void setAchievementCode(String achievementCode) {
        this.achievementCode = achievementCode;
    }
    
    public String getGameCode() {
        return gameCode;
    }

    public void setGameCode(String gameCode) {
        this.gameCode = gameCode;
    }
        
    public void registerGameCode() {
        PlayerProxy playerProxy = facebookBean.getCurrentPlayer();
        Player player = playerProxy.getPlayer();
        try {
            codeRegistrationService.registerGameCode(player, gameCode);
            String infoMessage = messageSource.getMessage("game.code.registered", new Object[]{1}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, infoMessage, infoMessage);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        }
    }
    
    public void registerAchievementCode() {
        PlayerProxy playerProxy = facebookBean.getCurrentPlayer();
        Player player = playerProxy.getPlayer();
        try {
            codeRegistrationService.registerAchievementCode(player, achievementCode);
            String infoMessage = messageSource.getMessage("achievement.code.registered", new Object[]{1}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, infoMessage, infoMessage);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        }
    }
    
}
