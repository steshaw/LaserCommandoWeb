/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.player.Player;
import au.com.lasercommando.Constants;
import au.com.lasercommando.model.WeaponEnum;
import au.com.lasercommando.model.event.*;
import au.com.lasercommando.model.map.Map;
import au.com.lasercommando.model.map.MapCategoryEnum;
import au.com.lasercommando.model.product.PayPalProduct;
import au.com.lasercommando.model.product.ProductOrder;
import au.com.lasercommando.service.CalculateCostService;
import au.com.lasercommando.service.CheckOutService;
import au.com.lasercommando.service.EventService;
import au.com.lasercommando.service.ProductService;
import au.com.lasercommando.util.DateUtil;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author KobusVM
 */
@Controller
@Scope("session")
public class EventSelectionBean implements java.io.Serializable {

    private List<EventProxy> availableEventsList;
    private List<EventProxy> existingEventsList;
    private List<EventProxy> specialEventsList;
    private List<EventProxy> filteredEventsList;
    private EventTypeEnum selectedEventType;
    private Map selectedMap;
    private EventProxy selectedEvent;
    private Payment selectedPayment;
    private Player currentPlayer;
    private PayPalProduct selectedPayPalProduct;
    private Collection<PayPalProduct> allProductsList;
    private int joiningMorePlayersToEvent;
    private Contact contact;
    private double totalPlayerCost;
    private double totalExtrasCost;
    private double totalCost;
    private double discount;
    @Value("${paypal.express.checkout.url}")
    private String paypalExpressCheckoutUrl;
    @Value("${price.per.player}")
    private double pricePerPlayer;
    private List<ProductOrder> productOrderList;
    @Resource
    private EventService eventService;
    @Resource
    private CheckOutService checkOutService;
    @Resource
    private MessageSource messageSource;
    @Resource
    private CalculateCostService calculateCostService;
    @Resource
    private ProductService productService;

    public List<EventProxy> getSpecialEventsList() {
        return specialEventsList;
    }

    public void setSpecialEventsList(List<EventProxy> specialEventsList) {
        this.specialEventsList = specialEventsList;
    }

    public List<EventProxy> getFilteredEventsList() {
        return filteredEventsList;
    }

    public void setFilteredEventsList(List<EventProxy> filteredEventsList) {
        this.filteredEventsList = filteredEventsList;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
    
    public double getTotalExtrasCost() {
        return totalExtrasCost;
    }

    public void setTotalExtrasCost(double totalExtrasCost) {
        this.totalExtrasCost = totalExtrasCost;
    }

    public List<ProductOrder> getProductOrderList() {
        return productOrderList;
    }

    public void setProductOrderList(List<ProductOrder> productOrderList) {
        this.productOrderList = productOrderList;
    }
    
    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotalPlayerCost() {
        return totalPlayerCost;
    }

    public void setTotalPlayerCost(double totalPlayerCost) {
        this.totalPlayerCost = totalPlayerCost;
    }
    
    public Payment getSelectedPayment() {
        return selectedPayment;
    }

    public void setSelectedPayment(Payment selectedPayment) {
        this.selectedPayment = selectedPayment;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public int getJoiningMorePlayersToEvent() {
        return joiningMorePlayersToEvent;
    }

    public void setJoiningMorePlayersToEvent(int joiningMorePlayersToEvent) {
        this.joiningMorePlayersToEvent = joiningMorePlayersToEvent;
    }

    public PayPalProduct getSelectedPayPalProduct() {
        return selectedPayPalProduct;
    }

    public void setSelectedPayPalProduct(PayPalProduct selectedPayPalProduct) {
        this.selectedPayPalProduct = selectedPayPalProduct;
    }

    public List<EventProxy> getAvailableEventsList() {
        return availableEventsList;
    }

    public void setAvailableEventsList(List<EventProxy> availableEventsList) {
        this.availableEventsList = availableEventsList;
    }

    public List<EventProxy> getExistingEventsList() {
        return existingEventsList.subList(0, 16);
    }

    public void setExistingEventsList(List<EventProxy> existingEventsList) {
        this.existingEventsList = existingEventsList;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public WeaponEnum[] getWeaponList() {
        //@todo only return weapons that are in stock
        return WeaponEnum.values();
    }

    public Collection<PayPalProduct> getAllProductsList() {
        return allProductsList;
    }

    public MapCategoryEnum[] getMapCategoryList() {
        return MapCategoryEnum.values();
    }

    public EventProxy getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(EventProxy selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public Map getSelectedMap() {
        return selectedMap;
    }

    public void setSelectedMap(Map selectedMap) {
        this.selectedMap = selectedMap;
    }

    public EventTypeEnum getSelectedEventType() {
        return selectedEventType;
    }

    public void setSelectedEventType(EventTypeEnum selectedEventType) {
        this.selectedEventType = selectedEventType;
    }

    public EventTypeEnum[] getEventTypeList() {
        return EventTypeEnum.values();
    }

    public AverageAgeEnum[] getAverageAgeList() {
        return AverageAgeEnum.values();
    }

    @PostConstruct
    public void init() {
        joiningMorePlayersToEvent = 1;        
        selectedEvent = new EventProxy(new Event());
        contact = new Contact();
        productOrderList = productService.findAll();
    }

    private void resetFields() {
        contact.setFirstname(null);
        contact.setSurname(null);
        contact.setId(java.util.UUID.randomUUID().toString());
        contact.setEmail(null);
        contact.setTelephoneNumber(null);
        contact.setGroupName(null);
        contact.setHowDidYouFindUs(null);
    }
    
    public void calculateCost() {
        totalPlayerCost = calculateCostService.calculateTotalCost(joiningMorePlayersToEvent, selectedEvent.getEvent().getPricePerPlayer());
        discount = calculateCostService.calculateDiscounts(joiningMorePlayersToEvent, selectedEvent.getEvent().getPricePerPlayer(), totalPlayerCost);
        totalPlayerCost = totalPlayerCost - discount;
    }
    
    public void calculateExtrasCost() {
        totalExtrasCost = calculateCostService.calculateExtrasCost(productOrderList);
        totalCost = totalPlayerCost + totalExtrasCost;
    }
    
    public String showExtras() {
        calculateExtrasCost();
        return "extras.jsf?faces-redirect=true";
    }

    public void selectNewGame() {
        availableEventsList = eventService.findAvailableEvents();
    }

    public String selectNewGameFromQuote() {
        selectNewGame();
        return "/bookgame/newgame/index.jsf?faces-redirect=true";
    }

    public void selectSpecialEvent() {
        specialEventsList = eventService.findSpecialEvents();
    }

    public void selectExistingGame() {
        existingEventsList = eventService.findExistingEvents();
    }

    public String selectExistingGameFromQuote() {
        selectExistingGame();
        return "/bookgame/existinggame/index.jsf?faces-redirect=true";
    }

    public String startBookingProcess(EventProxy eventProxy) {
        this.selectedEvent = eventProxy;
        totalPlayerCost = calculateCostService.calculateTotalCost(joiningMorePlayersToEvent, selectedEvent.getEvent().getPricePerPlayer());
        return "contact.jsf?faces-redirect=true";
    }

    public String startQuoteProcess() {
        Event event = new Event();

        event.setName("Quote");
        event.setPrivateEvent(false);
        event.setPricePerPlayer(pricePerPlayer);
        joiningMorePlayersToEvent = 1;
        
        this.selectedEvent = new EventProxy(event);
        calculateCost();
        return "/quotegame/game.jsf?faces-redirect=true";
    }

    public String startQuickJoiningProcess(EventProxy eventProxy) {
        startJoiningProcess(eventProxy);
        return "/bookgame/existinggame/contact.jsf?faces-redirect=true";
    }

    public String startJoiningProcess(EventProxy eventProxy) {
        this.selectedEvent = eventProxy;
        totalPlayerCost = calculateCostService.calculateTotalCost(joiningMorePlayersToEvent, selectedEvent.getEvent().getPricePerPlayer());
        String error = messageSource.getMessage("too.late.for.event", new Object[]{}, Locale.US);
        Date now = new Date();
        Date nowTrimmed = DateUtil.trim(now);
        Date eventDateTrimmed = DateUtil.trim(selectedEvent.getEvent().getDate());

        //check if the event is today
        if (nowTrimmed.equals(eventDateTrimmed)) {
            //it is today, check the time of the event
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            int currentHour = cal.get(Calendar.HOUR_OF_DAY);
            SessionEnum session = SessionEnum.findEnum(selectedEvent.getEvent().getSession());
            int eventStartHour = session.getStartHour();

            if (currentHour > eventStartHour) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return null;
            }
            
        } else if (nowTrimmed.after(eventDateTrimmed)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return null;
        }

        if (selectedEvent.getEvent().getPlayersJoined() >= selectedEvent.getEvent().getMaxPlayerCount()) {
            error = messageSource.getMessage("event.full", new Object[]{}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return null;
        }

        //validations passed so allow to join the event
        currentPlayer = new Player();
        return "contact.jsf?faces-redirect=true";
        
    }    

    public void joinCheckOut() {
        if (validateGameDetails() && validateJoinDetails()) {
            contact.setEventCreator(false);
            checkOut();
        }
    }

    public void bookCheckOut() {
        if (validateGameDetails() && validateBookDetails()) {
            contact.setEventCreator(true);
            contact.setGroupName(contact.getFirstname() + " " + contact.getSurname());
            checkOut();
        }
    }

    private boolean validateGameDetails() {
        int maxPlayerCount = selectedEvent.getEvent().getMaxPlayerCount() - selectedEvent.getEvent().getPlayersJoined();
        if (maxPlayerCount > Constants.MAX_PLAYERS) {
            String error = messageSource.getMessage("too.many.players", new Object[]{Constants.MAX_PLAYERS}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return false;
        } else if (maxPlayerCount < Constants.MIN_PLAYERS) {
            String error = messageSource.getMessage("too.few.players", new Object[]{Constants.MIN_PLAYERS}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return false;
        }
        return true;
    }

    private boolean validateJoinDetails() {
        int maxPlayerCount = selectedEvent.getEvent().getMaxPlayerCount() - selectedEvent.getEvent().getPlayersJoined();
        if (joiningMorePlayersToEvent > maxPlayerCount) {
            String error = messageSource.getMessage("pay.too.many", new Object[]{joiningMorePlayersToEvent, maxPlayerCount}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return false;
        } else if (joiningMorePlayersToEvent < 1) {
            String error = messageSource.getMessage("pay.too.few", new Object[]{joiningMorePlayersToEvent, 1}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return false;
        }
        return true;
    }

    private boolean validateBookDetails() {
        int maxPlayerCount = selectedEvent.getEvent().getMaxPlayerCount() - selectedEvent.getEvent().getPlayersJoined();
        if (joiningMorePlayersToEvent > maxPlayerCount) {
            String error = messageSource.getMessage("pay.too.many", new Object[]{joiningMorePlayersToEvent, maxPlayerCount}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return false;
        } else if (joiningMorePlayersToEvent < 8) {
            String error = messageSource.getMessage("pay.too.few", new Object[]{joiningMorePlayersToEvent, 8}, Locale.US);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return false;
        }
        return true;
    }

    private void checkOut() {
        try {
            String resultToken = checkOutService.setExpressCheckout(selectedEvent.getEvent(), joiningMorePlayersToEvent, contact, productOrderList).getToken();
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(paypalExpressCheckoutUrl + resultToken);
            resetFields();
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void changePlayersJoined() {
        if (selectedEvent.getEvent().getMaxPlayerCount() > Constants.MAX_PLAYERS) {
            selectedEvent.getEvent().setMaxPlayerCount(Constants.MAX_PLAYERS);
        } else if (selectedEvent.getEvent().getMaxPlayerCount() < Constants.MIN_PLAYERS) {
            selectedEvent.getEvent().setMaxPlayerCount(Constants.MIN_PLAYERS);
        }


        selectedEvent.getEvent().setPlayersJoined(selectedEvent.getEvent().getMaxPlayerCount());
    }

    public void populateEventLocation() {
    }

    public void populateSelectedPayPalProduct(String payPalProductId) {
        for (PayPalProduct payPalProduct : allProductsList) {
            if (payPalProduct.getId().equals(payPalProductId)) {
                selectedPayPalProduct = payPalProduct;
            }
        }
    }
}
