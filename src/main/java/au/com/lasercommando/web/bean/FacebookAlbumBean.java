/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.gallery.Album;
import au.com.lasercommando.service.AlbumService;
import au.com.lasercommando.web.util.FacebookAccessToken;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.Photo;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author KobusVM
 */
@Controller
@Scope("globalSession")
public class FacebookAlbumBean implements java.io.Serializable {

    private static Logger logger = Logger.getLogger(FacebookAlbumBean.class);
    @Value("${facebook.access.token.url}")
    private String facebookAccessTokenUrl;
    @Value("${facebook.app.id}")
    private String facebookAppId;
    @Value("${facebook.app.secret}")
    private String facebookAppSecret;
    @Value("${facebook.current.album.page.id}")
    private String facebookCurrentAlbumPageId;
    private List<Photo> photoList;
    private Photo selectedPhoto;
    @Resource
    private AlbumService albumService;

    public Photo getSelectedPhoto() {
        return selectedPhoto;
    }

    public void setSelectedPhoto(Photo selectedPhoto) {
        this.selectedPhoto = selectedPhoto;
    }

    public List<Photo> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<Photo> photoList) {
        this.photoList = photoList;
    }

    
    @PostConstruct
    public void init() {
        photoList = new ArrayList<Photo>();
        List<Album> albumList = albumService.findAll();
        if (albumList != null && (!albumList.isEmpty())) {
            facebookCurrentAlbumPageId = albumList.get(0).getCode();
        }

        facebookAccessTokenUrl = facebookAccessTokenUrl.replace("{0}", facebookAppId);
        facebookAccessTokenUrl = facebookAccessTokenUrl.replace("{1}", facebookAppSecret);

        //load facebook pictures
        String accessToken = FacebookAccessToken.retrieveAccessToken(facebookAccessTokenUrl);
        if (!accessToken.equals("")) {
            try {
                FacebookClient facebookClient = new DefaultFacebookClient(accessToken);
                Connection<Photo> myPhotos = facebookClient.fetchConnection(facebookCurrentAlbumPageId + "/photos", Photo.class);
                for (List<Photo> photos : myPhotos) {
                    for (Photo photo : photos) {
                        photoList.add(photo);
                    }
                }
            } catch (Exception ex) {
                logger.error("Reading access token failed for url " + facebookAccessTokenUrl, ex);
            }
        }
    }
}
