/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.service.PostService;
import au.com.lasercommando.web.util.FacebookAccessToken;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.Post;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;

/**
 *
 * @author KobusVM
 */
@Controller
@Scope("session")
public class FacebookFeedBean implements java.io.Serializable {

    private static Logger logger = Logger.getLogger(FacebookFeedBean.class);
    private String latestNews;
    private String latestNewsImageUrl;
    @Value("${facebook.access.token.url}")
    private String facebookAccessTokenUrl;
    @Value("${facebook.app.id}")
    private String facebookAppId;
    @Value("${facebook.app.secret}")
    private String facebookAppSecret;
    @Value("${facebook.home.page.id}")
    private String facebookHomePageId;
    @Resource(name = "taskExecutorFacebookFeed")
    private TaskExecutor taskExecutor;
    @Resource
    private PostService postService;

    public String getLatestNews() {
        return latestNews;
    }

    public void setLatestNews(String latestNews) {
        this.latestNews = latestNews;
    }

    public String getLatestNewsImageUrl() {
        return latestNewsImageUrl;
    }

    public void setLatestNewsImageUrl(String latestNewsImageUrl) {
        this.latestNewsImageUrl = latestNewsImageUrl;
    }

    @PostConstruct
    public void init() {
        facebookAccessTokenUrl = facebookAccessTokenUrl.replace("{0}", facebookAppId);
        facebookAccessTokenUrl = facebookAccessTokenUrl.replace("{1}", facebookAppSecret);

        //load facebook news in a thread
        taskExecutor.execute(new FacebookFeedTask(facebookAccessTokenUrl, facebookHomePageId));

        //load facebook news from database, because thread might take some time or timeout
        Post post = postService.findFirst();
        latestNews = post.getMessage();
        latestNewsImageUrl = post.getPicture();
    }

    private final class FacebookFeedTask implements Runnable {

        private String facebookAccessTokenUrl;
        private String facebookHomePageId;

        public FacebookFeedTask(String facebookAccessTokenUrl, String facebookHomePageId) {
            this.facebookAccessTokenUrl = facebookAccessTokenUrl;
            this.facebookHomePageId = facebookHomePageId;
        }

        @Override
        public void run() {
            String accessToken = FacebookAccessToken.retrieveAccessToken(facebookAccessTokenUrl);
            if (!accessToken.equals("")) {
                try {
                    FacebookClient facebookClient = new DefaultFacebookClient(accessToken);
                    Connection<Post> myFeed = facebookClient.fetchConnection(facebookHomePageId + "/feed", Post.class);
                    for (List<Post> myFeedConnectionPage : myFeed) {
                        for (Post post : myFeedConnectionPage) {
                            if (post.getMessage() != null) {
                                postService.save(post);
                                return;
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Reading access token failed for url " + facebookAccessTokenUrl, ex);
                }
            }
        }
    }
}
