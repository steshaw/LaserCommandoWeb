/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Kobus
 */
@Controller
@Scope("globalSession")
public class WebConstantsBean implements java.io.Serializable, Constants {
    @Value("${paypal.button.url}")
    private String payPalButtonUrl;
    @Value("${paypal.cart.key}")
    private String payPalCartKey;
    @Value("${enviro}")
    private String environment;

    public String getPayPalCartKey() {
        return payPalCartKey;
    }

    public void setPayPalCartKey(String payPalCartKey) {
        this.payPalCartKey = payPalCartKey;
    }

    public boolean isDevEnvironment() {
        return environment.equals("DEV");
    }

    public String getPayPalButtonUrl() {
        return payPalButtonUrl;
    }
    
    public String getDecimalFormat() {
        return DECIMAL_FORMAT;
    }
    
    public int getMaxPlayers() {
        return MAX_PLAYERS;
    }

    public int getMinPlayers() {
        return MIN_PLAYERS;
    }

    public String getTimeZone() {
        return TIME_ZONE;
    }
    
    public String getDateFormat() {
        return DATE_FORMAT;
    }

    public String getDateFormatJustNumber() {
        return DATE_FORMAT_JUST_NUMBERS;
    }

    public int getAutoPlayInterval() {
        return CAROUSEL_AUTO_PLAY_INTERVAL;
    }    

    public int getAutoPlayIntervalLong() {
        return CAROUSEL_AUTO_PLAY_INTERVAL_LONG;
    }    
    
    public int getEffectDuration() {
        return CAROUSEL_EFFECT_DURATON;
    }
}
