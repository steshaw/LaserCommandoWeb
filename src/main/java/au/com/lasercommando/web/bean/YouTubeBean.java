/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.gallery.Movie;
import au.com.lasercommando.service.MovieService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author KobusVM
 */
@Controller
@Scope("request")
public class YouTubeBean {
    @Value("${youtube.home.page}")
    private String youTubeHomePage;
    private List<Movie> movieList;
    @Resource
    private MovieService movieService;

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }

    public String getYouTubeHomePage() {
        return youTubeHomePage;
    }

    public void setYouTubeHomePage(String youTubeHomePage) {
        this.youTubeHomePage = youTubeHomePage;
    }

    @PostConstruct
    public void init() {
        movieList = movieService.findAll();
    }
}
