/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.player.AchievementEnum;
import au.com.lasercommando.model.player.GenAchievement;
import au.com.lasercommando.service.GenAchievementService;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author dutoitk
 */
@Controller
@Scope("session")
public class ManageAchievementBean implements java.io.Serializable {
    private GenAchievement selectedGenAchievement;
    private AchievementEnum selectedAchievement;
    private int totalAvailable;
    @Resource
    private GenAchievementService genAchievementService;

    public int getTotalAvailable() {
        return totalAvailable;
    }

    public void setTotalAvailable(int totalAvailable) {
        this.totalAvailable = totalAvailable;
    }
    
    public AchievementEnum getSelectedAchievement() {
        return selectedAchievement;
    }

    public void setSelectedAchievement(AchievementEnum selectedAchievement) {
        this.selectedAchievement = selectedAchievement;
    }
    
    public GenAchievement getSelectedGenAchievement() {
        return selectedGenAchievement;
    }

    public void setSelectedGenAchievement(GenAchievement selectedGenAchievement) {
        this.selectedGenAchievement = selectedGenAchievement;
    }
        
    public AchievementEnum[] getAchievementList() {
        return AchievementEnum.values();
    }
    
    @PostConstruct
    public void init() {
        totalAvailable = 1;
    }
    
    public String newAchievement() {
        return "achievement/createachievement.jsf?faces-redirect=true";
    }
    
    public String generate() {
        selectedGenAchievement = new GenAchievement();
        selectedGenAchievement.setAchievementCode(selectedAchievement.getCode());
        selectedGenAchievement.setTotalAvailable(totalAvailable);
        genAchievementService.save(selectedGenAchievement);

        return "achievementAlternateId.jsf?faces-redirect=true";
    }    
}
