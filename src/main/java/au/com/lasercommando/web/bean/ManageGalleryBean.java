/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.gallery.Album;
import au.com.lasercommando.model.gallery.Movie;
import au.com.lasercommando.service.AlbumService;
import au.com.lasercommando.service.MovieService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class ManageGalleryBean {
    private List<Movie> movieList;
    private List<Album> albumList;
    @Resource
    private MovieService movieService;
    @Resource
    private AlbumService albumService;

    public List<Album> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(List<Album> albumList) {
        this.albumList = albumList;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }
    
    @PostConstruct
    public void init() {
        movieList = movieService.findAll();
//        movieList.add(new Movie("fvZr4NwHMPg", "Zillmere state school fete 1 September"));
//        movieList.add(new Movie("oWtq_qWIQlw", "Zillmere state school fete 1 September"));
//        movieList.add(new Movie("KQIRNcCxMJs", "Zillmere state school fete 1 September"));
//        movieList.add(new Movie("sJAjMH4Yddw", "Zillmere state school fete 1 September"));
//        movieService.saveAll(movieList);

        albumList = albumService.findAll();
//        albumList.add(new Album("355420024539814", "Zillmere state school fete 1 September"));
//        albumService.saveAll(albumList);
    }

    public void save() {
        movieService.deleteAll();
        movieService.saveAll(movieList);

        albumService.deleteAll();
        albumService.saveAll(albumList);

        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Gallery changes saved", "Gallery changes saved");
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }
}
