/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.player.Achievement;
import au.com.lasercommando.model.player.AchievementEnum;
import au.com.lasercommando.model.player.Player;
import au.com.lasercommando.model.player.PlayerProxy;
import au.com.lasercommando.service.PlayerService;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author KobusVM
 */
@Controller
@Scope("session")
public class FacebookBean implements java.io.Serializable {

    private static Logger logger = Logger.getLogger(FacebookBean.class);
    @Resource
    private PlayerService playerService;
    @Value("${facebook.connect.url}")
    private String facebookConnectUrl;
    @Value("${facebook.home.page}")
    private String facebookHomePage;
    @Value("${facebook.app.id}")
    private String facebookAppId;
    @Value("${facebook.app.secret}")
    private String facebookAppSecret;
    @Value("${facebook.verify.url}")
    private String facebookVerifyUrl;
    private String token;
    private boolean loggedIn;
    private PlayerProxy playerProxy;
    @Resource
    private MessageSource messageSource;

    public String getFacebookHomePage() {
        return facebookHomePage;
    }

    public PlayerProxy getCurrentPlayer() {
        return playerProxy;
    }

    public void setCurrentPlayer(PlayerProxy playerProxy) {
        this.playerProxy = playerProxy;
    }

    public String getFacebookVerifyUrl() {
        return facebookVerifyUrl;
    }

    public String getFacebookAppSecret() {
        return facebookAppSecret;
    }

    public String getFacebookAppId() {
        return facebookAppId;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @PostConstruct
    public void init() {
        facebookVerifyUrl = facebookVerifyUrl.replace("{0}", facebookAppId);
        facebookVerifyUrl = facebookVerifyUrl.replace("{1}", facebookAppSecret);
    }

    public void loadUser() {
        logger.info("Loading user details");
        FacebookClient facebookClient = new DefaultFacebookClient(token);
        User user = facebookClient.fetchObject("me", User.class);

        logger.info("Finding player in database");
        playerProxy = playerService.findOne(user.getId());
        if (playerProxy == null) {
            //first time logging in with facebook, create new player
            logger.info("Player not found, creating player");
            playerProxy = new PlayerProxy(new Player());
            playerProxy.getPlayer().setId(user.getId());
        }
        createAchievements(playerProxy);

        playerProxy.getPlayer().setName(user.getFirstName());
        if (user.getMiddleName() == null) {
            playerProxy.getPlayer().setSurname(user.getLastName());
        } else {
            playerProxy.getPlayer().setSurname(user.getMiddleName() + " " + user.getLastName());
        }

        //only change e-mail if the new e-mail from facebook actually has something in it
        if (user.getEmail() != null) {
            playerProxy.getPlayer().setEmail(user.getEmail());
        }
        playerProxy.getPlayer().setGender(user.getGender());
        playerService.save(playerProxy.getPlayer());

        loggedIn = true;
    }

    public String reloadPlayer() {
        if (playerProxy != null) {
            playerProxy = playerService.findOne(playerProxy.getPlayer().getId());
        }

        return "rank.jsf?faces-redirect=true";
    }

    private void createAchievements(PlayerProxy playerProxy) {
        if (playerProxy.getPlayer().getAchievementList().isEmpty()) {
            AchievementEnum[] achievementEnumList = AchievementEnum.values();
            int length = achievementEnumList.length;
            for (int i = 0; i < length; i++) {
                AchievementEnum achievementEnum = achievementEnumList[i];
                if (achievementEnum.isActive()) {
                    //only add active enums
                    Achievement achievement = new Achievement();
                    achievement.setAchievementCode(achievementEnum.getCode());
                    achievement.setDescription(achievementEnum.getDescription());
                    achievement.setName(achievementEnum.getName());
                    achievement.setUrl(achievementEnum.getUrl());
                    achievement.setNumberRequired(achievementEnum.getNumberRequired());
                    playerProxy.getPlayer().addAchievement(achievement);
                }
            }
        }
    }

    public void login() {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(facebookConnectUrl + facebookAppId);
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void update() {
        playerService.save(playerProxy.getPlayer());

        String error = messageSource.getMessage("profile.updated", new Object[]{}, Locale.US);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, error, error);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
