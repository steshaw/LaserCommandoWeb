/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.service.BatchBillService;
import javax.annotation.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Kobus
 */
@Controller
@Scope("view")
public class HomeBean implements java.io.Serializable {
    @Resource
    private BatchBillService batchBillService;

    public void sendNewBookingMail() {
        batchBillService.execute();
    }
}
