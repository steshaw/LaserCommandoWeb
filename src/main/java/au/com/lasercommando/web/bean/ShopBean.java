/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.bean;

import au.com.lasercommando.model.product.PayPalProduct;
import au.com.lasercommando.service.PayPalProductService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 *
 * @author dutoitk
 */
@Controller
@Scope("view")
public class ShopBean implements java.io.Serializable {
    @Resource
    private PayPalProductService payPalProductService;
    private List<PayPalProduct> payPalProductList;
    private PayPalProduct selectedPayPalProduct;
    private String selectedOption;

    public String getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public PayPalProduct getSelectedPayPalProduct() {
        return selectedPayPalProduct;
    }

    public void setSelectedPayPalProduct(PayPalProduct selectedPayPalProduct) {
        this.selectedPayPalProduct = selectedPayPalProduct;
    }

    public List<PayPalProduct> getPayPalProductList() {
        return payPalProductList;
    }

    @PostConstruct
    public void init() {
        payPalProductList = payPalProductService.findAllActive();
    }
}
