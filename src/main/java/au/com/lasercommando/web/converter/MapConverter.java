/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.converter;

import au.com.lasercommando.model.map.Map;
import au.com.lasercommando.service.MapService;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.servlet.ServletContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author dutoitk
 */
@FacesConverter("mapConverter")
public class MapConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(sc);
        MapService mapService = context.getBean(MapService.class);
        Map map = mapService.findOne(value);
        return map;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object == null) {
            return "";
        } else {
            Map map = (Map) object;
            return map.getId();
        }
    }

}
