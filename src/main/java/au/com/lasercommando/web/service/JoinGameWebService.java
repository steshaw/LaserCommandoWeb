/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.service;

import au.com.lasercommando.model.event.EventProxy;
import au.com.lasercommando.service.EventService;
import au.com.lasercommando.web.bean.EventSelectionBean;
import javax.annotation.Resource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author dutoitk
 */
@Controller
@Scope("session")
public class JoinGameWebService implements java.io.Serializable {
    @Resource
    private EventService eventService;
    @Resource
    private EventSelectionBean eventSelectionBean;
    
    @RequestMapping(value = "/joinGame/{eventId}", method = RequestMethod.GET)
    public ModelAndView passwordChange(@PathVariable("eventId") String eventId) {
        RedirectView errorPage = new RedirectView("/bookgame/existinggame/joinGameErrorPage.jsf");
        RedirectView joinContactPage = new RedirectView("/bookgame/existinggame/contact.jsf");
        
        EventProxy eventProxy = eventService.findEvent(eventId);
        if (eventProxy == null) {
            return new ModelAndView(errorPage);
        }
        
        eventSelectionBean.setSelectedEvent(eventProxy);
        return new ModelAndView(joinContactPage);        
    }
}
