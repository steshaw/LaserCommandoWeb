/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.servlet;

import au.com.lasercommando.web.bean.YouTubeBean;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author KobusVM
 */
@WebServlet(name = "youtube", urlPatterns = {"/youtube"})
public class YouTubeRedirectServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(YouTubeRedirectServlet.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());

            YouTubeBean youTubeBean = ctx.getBean(YouTubeBean.class);
            response.sendRedirect(youTubeBean.getYouTubeHomePage());

        } catch (Exception ex) {
            logger.error("Connection to YouTube failed", ex);
            response.sendRedirect("/errors/youTubeReturnError.jsf");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet that accepts paypal return callback";
    }// </editor-fold>
}
