/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.servlet;

import au.com.lasercommando.model.event.*;
import au.com.lasercommando.model.map.AuthorEnum;
import au.com.lasercommando.service.CheckOutService;
import au.com.lasercommando.service.EventService;
import au.com.lasercommando.service.MailService;
import au.com.lasercommando.web.bean.EventSelectionBean;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author KobusVM
 */
@WebServlet(name = "payPalReturn", urlPatterns = {"/payPalReturn"})
public class PayPalReturnServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(PayPalReturnServlet.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());

        String token = request.getParameter("token");
        logger.info("token received " + token);
        String payerId = request.getParameter("PayerID");
        logger.info("payer id received " + payerId);

        CheckOutService checkOutService = ctx.getBean("checkOutService", CheckOutService.class);
        try {
            CheckOutResponse checkOutResponse = checkOutService.getExpressCheckout(token);
            if (!checkOutResponse.getToken().equals(token)) {
                throw new Exception("Tokens don't match");
            }
            checkOutService.doExpressCheckout(token, payerId, checkOutResponse.getTotalAmount());

            //save event.  all was fine with checkout
            logger.info("save event, all went fine with paypal");
            EventService eventService = ctx.getBean("eventService", EventService.class);
            EventProxy eventProxy = eventService.findEventByPaymentId(token);
            if (eventProxy == null) {
                throw new Exception("No event could be found with payment id of " + token);
            }
            Payment payment = eventService.findPayment(eventProxy.getEvent(), token);
            if (payment == null) {
                throw new Exception("No payment could be found with payment id of " + token);
            }

            logger.info("set event status to booked and add more players.  eventId:" + eventProxy.getEvent().getId());
            payment.setPaid(true);
            int originalEventStatus = eventProxy.getEvent().getStatus();
            int originalPlayersJoined = eventProxy.getEvent().getPlayersJoined();
            eventProxy.getEvent().setPlayersJoined(eventProxy.getEvent().getPlayersJoined() + payment.getPaidForPlayers());
            eventProxy.getEvent().setStatus(EventStatusEnum.BOOKED.getCode());
            eventService.save(eventProxy.getEvent());

            //set selected event to the event that was paid for
            EventSelectionBean eventSelectionBean = ctx.getBean("eventSelectionBean", EventSelectionBean.class);
            eventSelectionBean.setSelectedEvent(eventProxy);
            eventSelectionBean.setSelectedPayment(payment);

            //do this before changing status of booking, because that status
            //is used to determine if it is a new event or someone joining an
            //event
            mailBookingDetails(eventProxy.getEvent(), payment, ctx, originalEventStatus, originalPlayersJoined);

            response.sendRedirect("bookgame/payPalBookPage.jsf");
            //redirect to correct page
        } catch (Exception ex) {
            //redirect to error page
            logger.error("An error occured getting a response from PayPal", ex);
            response.sendRedirect("bookgame/payPalErrorPage.jsf");
        }
    }

    private void mailBookingDetails(Event event, Payment payment, WebApplicationContext ctx, 
            int originalEventStatus, int originalPlayersJoined) {
        try {
            MailService mailService = ctx.getBean("mailService", MailService.class);
            if (event.getAuthor() == AuthorEnum.LASER_COMMANDO.getCode()) {
                mailLaserCommandoBooking(event, payment, mailService, originalPlayersJoined);
            } else if (event.getAuthor() == AuthorEnum.CUSTOM.getCode()) {
                mailCustomBooking(event, payment, mailService, originalEventStatus);
            }
        } catch (Exception ex) {
            logger.error("An error occured sending a mail", ex);
        }
    }

    private void mailLaserCommandoBooking(Event event, Payment payment, MailService mailService, int originalPlayersJoined) {
        //tell the companies where we play we are coming, but only when the first person signs up
        if (originalPlayersJoined == 0) {
            mailService.sendLocationBookingMail(event, payment);
        }

        //send a welcome mail to the person that joined the event
//        mailService.sendJoiningMail(event, payment);
        mailService.sendNewBookingMail(event, payment);
        
        //the person might be joining a public event for a group of people, send an invitation
        mailService.sendNewInvitationMail(event, payment);
    }

    private void mailCustomBooking(Event event, Payment payment, MailService mailService, int originalEventStatus) {
        if (originalEventStatus == EventStatusEnum.AVAILABLE.getCode()) {
            //new event
            mailService.sendNewBookingMail(event, payment);
            mailService.sendNewInvitationMail(event, payment);
        } else {
            //someone joining an existing event
            mailService.sendJoiningMail(event, payment);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet that accepts paypal return callback";
    }// </editor-fold>
}
