/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.web.servlet;

import au.com.lasercommando.web.bean.FacebookBean;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author KobusVM
 */
@WebServlet(name = "facebookReturn", urlPatterns = {"/facebookReturn"})
public class FacebookReturnServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(FacebookReturnServlet.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());

            FacebookBean facebookBean = ctx.getBean(FacebookBean.class);
            String token = readTokenFromFacebook(request.getParameter("code"), ctx.getBean(FacebookBean.class));

            facebookBean.setToken(token);
            facebookBean.loadUser();
            response.sendRedirect("/");

        } catch (Exception ex) {
            logger.error("Connection to facebook failed", ex);
            response.sendRedirect("/errors/facebookReturnError.jsf");
        }
    }

    private String readTokenFromFacebook(String verificationCode, FacebookBean facebookBean) throws MalformedURLException, IOException {
        logger.info("verification code received " + verificationCode);
        String verifyUrl = facebookBean.getFacebookVerifyUrl();
//        verifyUrl = verifyUrl.replace("{0}", facebookBean.getFacebookAppId());
//        verifyUrl = verifyUrl.replace("{1}", facebookBean.getFacebookAppSecret());
        verifyUrl = verifyUrl.replace("{2}", verificationCode);
        String result = connect(verifyUrl);
        
        String splitResultEquals[] = result.split("=");
        String splitResultAmpersand[] = splitResultEquals[1].split("&");
        
        return splitResultAmpersand[0];
    }

    private String connect(String url) throws MalformedURLException, IOException {
        URL server = new URL(url);
        HttpURLConnection connection = null;
        String useProxy = System.getProperty("use.proxy");
        if (useProxy != null) {
            boolean useProxyBoolean = Boolean.parseBoolean(useProxy);
            if (useProxyBoolean) {
                String host = System.getProperty("http.proxyHost");
                int port = Integer.parseInt(System.getProperty("http.proxyPort"));
                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));
                connection = (HttpURLConnection) server.openConnection(proxy);
            }
        } else {
            connection = (HttpURLConnection) server.openConnection();            
        }

        connection.connect();
        InputStream in = connection.getInputStream();
        return readResponse(in);
    }

    private String readResponse(InputStream is) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(is);
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int result = bis.read();
        while (result != -1) {
            byte b = (byte) result;
            buf.write(b);
            result = bis.read();
        }

        return buf.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet that accepts paypal return callback";
    }// </editor-fold>
}
