/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import com.restfb.types.Post;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dutoitk
 */
@Repository
public interface PostDao extends MongoRepository<Post, String>, java.io.Serializable {

}