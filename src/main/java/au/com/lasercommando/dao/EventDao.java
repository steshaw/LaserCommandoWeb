/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import au.com.lasercommando.model.event.Event;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dutoitk
 */
@Repository
public interface EventDao extends MongoRepository<Event, String>, java.io.Serializable {
    @Query("{status:?0, date: {$gte:?1}, active:?2}")
    List<Event> findActiveEvents(int status, Date date, boolean active);

    @Query("{date: {$gte:?0}, active:?1}")
    List<Event> findActiveEvents(Date date, boolean active);
    
    
    @Query("{alternateId:?0}")
    Event findEventByAlternateId(String alternateId);

    @Query("{author:?0, date: {$gte : ?1, $lte: ?2}, map.name:?3}")
    List<Event> findEvents(int author, Date startDate, Date endDate, String mapName);
}
