/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package au.com.lasercommando.dao;

import au.com.lasercommando.model.gallery.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieDao extends MongoRepository<Movie, String>, java.io.Serializable {

}
