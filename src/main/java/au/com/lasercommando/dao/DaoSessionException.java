/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

/**
 *
 * @author dutoitk
 */
public class DaoSessionException extends RuntimeException {
    public DaoSessionException(String message) {
        super(message);
    }

    public DaoSessionException(Throwable cause) {
        super(cause);
    }

    public DaoSessionException(String message, Throwable cause) {
        super(message, cause);
    }
}
