/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import au.com.lasercommando.model.map.Map;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author KobusVM
 */
@Repository
public interface MapDao extends MongoRepository<Map, String>, java.io.Serializable {
    
}
