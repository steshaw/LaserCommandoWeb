/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import au.com.lasercommando.model.event.Contact;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dutoitk
 */
@Repository
public interface ContactDao extends MongoRepository<Contact, String>, java.io.Serializable {

}