/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import com.mongodb.Mongo;
import com.mongodb.MongoURI;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 *
 * @author dutoitk
 */
@Configuration
public class SpringMongoConfig extends AbstractMongoConfiguration {

    private static Logger logger = Logger.getLogger(SpringMongoConfig.class);
    @Value("${mongo.hq.url}")
    private String mongoHqUrl;
    @Value("${mongo.db.name}")
    private String databaseName;

    @Override
    public @Bean
    MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate;
        mongoTemplate = createMongoTemplate();
        return mongoTemplate;
    }

    private MongoTemplate createMongoTemplate() throws Exception {
        UserCredentials userCredentials = getUserCredentials();
        if (userCredentials == null) {
            return new MongoTemplate(mongo(), getDatabaseName());
        } else {
            return new MongoTemplate(mongo(), getDatabaseName(), userCredentials);
        }
    }

    @Override
    public @Bean
    Mongo mongo() throws Exception {
        return new Mongo(getMongoURI());
    }

    @Override
    public String getDatabaseName() {
        return databaseName;
    }

    @Override
    public UserCredentials getUserCredentials() {
        try {
            if (getMongoURI().getUsername() == null) {
                return null;
            } else {
                UserCredentials userCredentials = new UserCredentials(getMongoURI().getUsername(),
                        String.valueOf(getMongoURI().getPassword()));
                return userCredentials;
            }
        } catch (Exception ex) {
            logger.error(ex);
        }

        return null;
    }

    private MongoURI getMongoURI() throws Exception {
        MongoURI mongoURI = new MongoURI(mongoHqUrl);
        return mongoURI;

    }
}