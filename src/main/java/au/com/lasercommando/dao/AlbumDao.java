/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package au.com.lasercommando.dao;

import au.com.lasercommando.model.gallery.Album;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AlbumDao extends MongoRepository<Album, String>, java.io.Serializable {

}
