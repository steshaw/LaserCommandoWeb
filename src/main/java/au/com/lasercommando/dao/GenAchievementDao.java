/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import au.com.lasercommando.model.player.GenAchievement;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 *
 * @author KobusVM
 */
public interface GenAchievementDao extends MongoRepository<GenAchievement, String>, java.io.Serializable {
    @Query("{alternateId:?0}")
    GenAchievement findGenAchievementByAlternateId(String alternateId);
}
