/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import au.com.lasercommando.model.product.PayPalProduct;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dutoitk
 */
@Repository
public interface PayPalProductDao extends MongoRepository<PayPalProduct, String>, java.io.Serializable {
    @Query("{active:true}")
    List<PayPalProduct> findAllActive();
}
