/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.dao;

import au.com.lasercommando.model.Advert;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dutoitk
 */
@Repository
public interface AdvertDao extends MongoRepository<Advert, String>, java.io.Serializable {

}