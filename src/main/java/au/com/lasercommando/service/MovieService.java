/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package au.com.lasercommando.service;

import au.com.lasercommando.dao.MovieDao;
import au.com.lasercommando.model.gallery.Movie;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class MovieService implements java.io.Serializable {
    @Resource
    private MovieDao movieDao;

    public List<Movie> findAll() {
        return movieDao.findAll();
    }

    public void saveAll(List<Movie> movieList) {
        movieDao.save(movieList);
    }

    public void deleteAll() {
        movieDao.deleteAll();
    }
}
