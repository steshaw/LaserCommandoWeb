/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.model.product.Product;
import au.com.lasercommando.model.product.ProductOrder;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author KobusVM
 */
@Service
public class CalculateCostService implements java.io.Serializable {
    @Value("${discount.10.players}")
    private double discount10Players;
    @Value("${discount.20.players}")
    private double discount20Players;
    @Value("${discount.30.players}")
    private double discount30Players;
    @Value("${discount.40.players}")
    private double discount40Players;
    
    public double calculateTotalCost(int playersPayingFor, double pricePerPlayer) {
        double totalCost = playersPayingFor * pricePerPlayer;
        return totalCost;
    }

    public double calculateDiscounts(int playersPayingFor, double pricePerPlayer) {
        double totalCost = calculateTotalCost(playersPayingFor, pricePerPlayer);
        return calculateDiscounts(playersPayingFor, pricePerPlayer, totalCost);
    }
    
    public double calculateDiscounts(int playersPayingFor, double pricePerPlayer, double totalCost) {
        
        double totalDiscount = 0;
        if ((playersPayingFor >= 10) && (playersPayingFor < 20)) {
            totalDiscount = totalCost * discount10Players;
        } else if ((playersPayingFor >= 20) && (playersPayingFor < 30)) {
            totalDiscount = totalCost * discount20Players;
        } else if ((playersPayingFor >= 30) && (playersPayingFor < 40)) {
            totalDiscount = totalCost * discount30Players;
        } else if (playersPayingFor >= 40) {
            totalDiscount = totalCost * discount40Players;
        }

        return totalDiscount;
    }
    
    public double calculateExtrasCost(List<ProductOrder> productOrderList) {
        double extrasCost = 0;
        for (ProductOrder productOrder : productOrderList) {
            Product product = productOrder.getProduct();
            BigDecimal productTotal = product.getPrice().multiply(new BigDecimal(productOrder.getQuantity()));
            extrasCost = extrasCost + productTotal.doubleValue();
        }
        
        return extrasCost;
    }
    
}
