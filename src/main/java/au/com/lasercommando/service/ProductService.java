/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.ProductDao;
import au.com.lasercommando.model.product.Product;
import au.com.lasercommando.model.product.ProductOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author KobusVM
 */
@Service
public class ProductService implements java.io.Serializable {
    @Resource
    private ProductDao productDao;

    public void save(Product product) {
        productDao.save(product);
    }

    public List<ProductOrder> findAll() {
        List<ProductOrder> result = new ArrayList<ProductOrder>();
        List<Product> allProducts = productDao.findAll();
        for (Product product : allProducts) {
            ProductOrder productOrder = new ProductOrder(product);
            result.add(productOrder);
        }
        
        Collections.sort(result, new ProductOrderSorter());
        
        return result;
    }
}
