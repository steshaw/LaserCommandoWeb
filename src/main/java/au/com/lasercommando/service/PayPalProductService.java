/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.PayPalProductDao;
import au.com.lasercommando.model.product.PayPalProduct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class PayPalProductService implements java.io.Serializable  {
    @Resource
    private PayPalProductDao payPalProductDao;

    public List<PayPalProduct> findAll() {
        Sort sort = new Sort(Sort.Direction.ASC, "sortOrder");
        List<PayPalProduct> result = payPalProductDao.findAll(sort);
        return result;
    }
    
    public List<PayPalProduct> findAllActive() {
        List<PayPalProduct> result = payPalProductDao.findAllActive();
        Collections.sort(result, new SortPayPalProduct());
        return result;
    }

    public void save(PayPalProduct payPalProduct) {
        payPalProductDao.save(payPalProduct);
    }
}
