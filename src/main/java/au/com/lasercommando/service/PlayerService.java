/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.PlayerDao;
import au.com.lasercommando.model.player.Player;
import au.com.lasercommando.model.player.PlayerProxy;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author KobusVM
 */
@Service
public class PlayerService implements java.io.Serializable {
    @Resource
    private PlayerDao playerDao;
    
    public PlayerProxy findOne(String id) {
        Player player = playerDao.findOne(id);
        if (player == null) {
            return null;
        } else {
            return new PlayerProxy(playerDao.findOne(id));
        }
    }
    
    public Player save(Player player) {
        Player savedPlayer = playerDao.save(player);
        return savedPlayer;
    }
    
    
}
