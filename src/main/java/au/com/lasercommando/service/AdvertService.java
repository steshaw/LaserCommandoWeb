/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.AdvertDao;
import au.com.lasercommando.model.Advert;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class AdvertService implements java.io.Serializable {
    @Resource
    private AdvertDao advertDao;

    public List<Advert> findAll() {
        return advertDao.findAll();
    }
}
