/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.GenAchievementDao;
import au.com.lasercommando.model.player.GenAchievement;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author KobusVM
 */
@Service
public class GenAchievementService implements java.io.Serializable  {
    private static Logger logger = Logger.getLogger(GenAchievementService.class);
    @Resource
    private GenAchievementDao genAchievementDao;
    
    public void save(GenAchievement genAchievement) {
        genAchievementDao.save(genAchievement);
    }
    
    public GenAchievement findGenAchievementByAlternateId(String alternateId) {
        return genAchievementDao.findGenAchievementByAlternateId(alternateId);
    }
}
