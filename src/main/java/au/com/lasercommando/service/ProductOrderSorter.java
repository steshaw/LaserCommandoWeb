/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.model.product.ProductOrder;
import java.util.Comparator;

/**
 *
 * @author dutoitk
 */
public class ProductOrderSorter implements Comparator<ProductOrder> {

    @Override
    public int compare(ProductOrder o1, ProductOrder o2) {
        return (o1.getProduct().getSortOrder() < o2.getProduct().getSortOrder() ? -1 : (o1.getProduct().getSortOrder() == o2.getProduct().getSortOrder() ? 0 : 1));        
    }

}
