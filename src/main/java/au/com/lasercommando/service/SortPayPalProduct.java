/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.model.product.PayPalProduct;
import java.util.Comparator;

/**
 *
 * @author KobusVM
 */
public class SortPayPalProduct implements Comparator<PayPalProduct> {

    @Override
    public int compare(PayPalProduct o1, PayPalProduct o2) {
         return (o1.getSortOrder() < o2.getSortOrder() ? -1 : (o1.getSortOrder() == o2.getSortOrder() ? 0 : 1));
    }
    
}
