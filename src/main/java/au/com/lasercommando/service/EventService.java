/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.EventDao;
import au.com.lasercommando.model.event.Event;
import au.com.lasercommando.model.event.EventProxy;
import au.com.lasercommando.model.event.EventStatusEnum;
import au.com.lasercommando.model.event.Payment;
import au.com.lasercommando.util.DateUtil;
import java.util.*;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class EventService implements java.io.Serializable {

    private static Logger logger = Logger.getLogger(EventService.class);
    @Resource
    private EventDao eventDao;

    public List<EventProxy> findAll() {
        return wrapEvent(eventDao.findAll());
    }
    
    public List<EventProxy> findActiveEvents() {        
        List<Event> eventList = new ArrayList<Event>();
        eventList = eventDao.findActiveEvents(DateUtil.trim(new Date()), true);
        return wrapEvent(eventList);
    }
    
    public void save(Event event) {
        eventDao.save(event);
    }

    public EventProxy findEventByPaymentId(String paymentTokenId) {
        List<Event> events = eventDao.findAll();
        for (Event event : events) {
            List<Payment> paymentList = event.getPaymentList();
            for (Payment payment : paymentList) {
                if (payment.getId().equals(paymentTokenId)) {
                    //backwards compatibility
                    return wrapEvent(event);
                } else if (payment.getPaymentTokenId() != null && payment.getPaymentTokenId().equals(paymentTokenId)) {
                    //new way of saving the primary key from paypal
                    return wrapEvent(event);
                }
            }
        }
        
        return null;
    }
    
    public Payment findPayment(Event event, String paymentTokenId) {
        List<Payment> paymentList = event.getPaymentList();
        for (Payment payment : paymentList) {
            if (payment.getId().equals(paymentTokenId)) {
                //backwards compatibility
                return payment;
            } else if (payment.getPaymentTokenId() != null && payment.getPaymentTokenId().equals(paymentTokenId)) {
                return payment;
            }
        }
        
        return null;
    }

    public List<EventProxy> findEvents(int author, Date startDate, Date endDate, String mapName) {
        List<Event> eventList = new ArrayList<Event>();
        eventList = eventDao.findEvents(author, startDate, endDate, mapName);
        return wrapEvent(eventList);
        
    }

    public EventProxy findEventByAlternateId(String alternateId) {
        EventProxy eventProxy = null;
        Event event = eventDao.findEventByAlternateId(alternateId);
        if (event == null) {
            return null;
        } else {
            eventProxy = wrapEvent(event);
        }

        return eventProxy;
    }

    public EventProxy findEvent(String id) {
        Event event = eventDao.findOne(id);
        return wrapEvent(event);
    }

    public List<EventProxy> findSpecialEvents() {
        GregorianCalendar november3 = new GregorianCalendar(2012, 10, 3);

        List<Event> eventList = eventDao.findActiveEvents(EventStatusEnum.BOOKED.getCode(), DateUtil.trim(november3.getTime()), true);
        return wrapEvent(eventList);
    }

    public List<EventProxy> findExistingEvents() {
        List<EventProxy> events = findEvents(EventStatusEnum.BOOKED.getCode());
        return events;
    }

    public List<EventProxy> findAvailableEvents() {
        List<EventProxy> events = findEvents(EventStatusEnum.AVAILABLE.getCode());
        return events;
    }

    private List<EventProxy> findEvents(int statusCode) {
        List<Event> eventList = new ArrayList<Event>();
        eventList = eventDao.findActiveEvents(statusCode, DateUtil.trim(new Date()), true);
        return wrapEvent(eventList);
    }
    
    private List<EventProxy> wrapEvent(List<Event> eventList) {
        ArrayList<EventProxy> eventProxyList = new ArrayList<EventProxy>();
        for (Event event : eventList) {
            EventProxy eventProxy = wrapEvent(event);
            eventProxyList.add(eventProxy);
        }
        Collections.sort(eventProxyList, new EventsSorter());
        return eventProxyList;
    }
    
    private EventProxy wrapEvent(Event event) {
        EventProxy proxy = new EventProxy(event);
        return proxy;
    }
}
