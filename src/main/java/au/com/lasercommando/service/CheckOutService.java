/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.Constants;
import au.com.lasercommando.model.event.CheckOutResponse;
import au.com.lasercommando.model.event.Contact;
import au.com.lasercommando.model.event.Event;
import au.com.lasercommando.model.event.Payment;
import au.com.lasercommando.model.product.ProductOrder;
import com.nvp.codegenerator.ECDoExpressCheckout;
import com.nvp.codegenerator.ECGetExpressCheckout;
import com.nvp.codegenerator.ECSetExpressCheckout;
import com.paypal.sdk.core.nvp.NVPDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class CheckOutService implements java.io.Serializable {

    private static Logger logger = Logger.getLogger(CheckOutService.class);
    @Value("${paypal.api.username}")
    private String payPalApiUsername;
    @Value("${paypal.api.password}")
    private String payPalApiPassword;
    @Value("${paypal.api.signature}")
    private String payPalApiSignature;
    @Value("${paypal.environment}")
    private String payPalEnvironment;
    @Value("${paypal.api.payment.type}")
    private String paypalApiPaymentType;
    @Value("${paypal.api.version}")
    private String payPalApiVersion;
    @Value("${paypal.api.currency}")
    private String payPalApiCurrency;
    @Value("${paypal.api.return.url}")
    private String payPalApiReturnUrl;
    @Value("${paypal.api.cancel.url}")
    private String payPalApiCancelUrl;
    @Resource
    private EventService eventService;
    @Resource
    private CalculateCostService calculateCostService;

    public CheckOutResponse doExpressCheckout(String token, String payerId, String amount) throws Exception {
        logger.info("start doExpressCheckout");
        ECDoExpressCheckout doExpressCheckout = new ECDoExpressCheckout();
        NVPDecoder response = doExpressCheckout.ECDoExpressCheckoutCode(
                token,
                payerId,
                amount,
                paypalApiPaymentType,
                payPalApiCurrency,
                payPalApiUsername,
                payPalApiPassword,
                payPalApiSignature,
                payPalEnvironment,
                payPalApiVersion);
        validateResponse(response);
        logger.info("end doExpressCheckout");
        return new CheckOutResponse(response);
    }

    public CheckOutResponse getExpressCheckout(String token) throws Exception {
        logger.info("start getExpressCheckout");
        ECGetExpressCheckout expressCheckoutCode = new ECGetExpressCheckout();
        NVPDecoder response = expressCheckoutCode.ECGetExpressCheckoutCode(
                payPalApiUsername,
                payPalApiPassword,
                payPalApiSignature,
                payPalEnvironment,
                payPalApiVersion,
                token);
        validateResponse(response);
        logger.info("end getExpressCheckout");
        return new CheckOutResponse(response);
    }

    public CheckOutResponse setExpressCheckout(Event event, 
                                               int joiningMorePlayersToEvent, 
                                               Contact contact,
                                               List<ProductOrder> productOrderList) throws Exception {
        logger.info("start setExpressCheckout");
        
        List<ProductOrder> newProductOrderList = new ArrayList<ProductOrder>();
        for (ProductOrder productOrder : productOrderList) {
            if (productOrder.getQuantity() > 0) {
                newProductOrderList.add(productOrder);
            }
        }
        
        if (joiningMorePlayersToEvent <= 0) {
            throw new Exception("Please pay for at least 1 player");
        }

        DecimalFormat df = new DecimalFormat(Constants.DECIMAL_FORMAT);
        double discountAmount = calculateCostService.calculateDiscounts(joiningMorePlayersToEvent, event.getPricePerPlayer());
        double totalPlayerCost = calculateCostService.calculateTotalCost(joiningMorePlayersToEvent, event.getPricePerPlayer());
        double totalExtrasCost = calculateCostService.calculateExtrasCost(productOrderList);
        double totalCost = (totalPlayerCost - discountAmount) + totalExtrasCost;

        ECSetExpressCheckout setExpressCheckout = new ECSetExpressCheckout();
        NVPDecoder response = setExpressCheckout.ECSetExpressCheckoutCode(
                payPalApiReturnUrl,
                payPalApiCancelUrl,
                df.format(discountAmount * -1),
                df.format(totalCost),
                paypalApiPaymentType,
                payPalApiCurrency,
                payPalApiUsername,
                payPalApiPassword,
                payPalApiSignature,
                payPalEnvironment,
                String.valueOf(event.getPricePerPlayer()),
                "Laser Commando Booking - " + event.getName(),
                String.valueOf(joiningMorePlayersToEvent), payPalApiVersion,
                newProductOrderList);
        validateResponse(response);

        logger.info("setExpressCheckout add payment to event");
        Payment payment = new Payment();
        CheckOutResponse checkOutResponse = new CheckOutResponse(response);
        payment.setPaymentTokenId(checkOutResponse.getToken());
        payment.setPaidForPlayers(joiningMorePlayersToEvent);
        payment.setDiscountAmount(discountAmount);
        payment.setTotalPlayerCost(totalPlayerCost);
        payment.setTotalExtrasCost(totalExtrasCost);
        payment.setTotalCost(totalCost);
        payment.setContact(contact);
        payment.setProductOrderList(newProductOrderList);
        event.addPayment(payment);
        eventService.save(event);

        logger.info("end setExpressCheckout");
        return checkOutResponse;
    }

    private void validateResponse(NVPDecoder response) throws Exception {
        StringBuilder error = new StringBuilder("Contacting PayPal caused the following error\n");
        String successFailure = response.get("ACK");
        if (successFailure == null) {
            error.append("No response from PayPal").append("\n");
            throw new Exception(error.toString());
        } else if (!successFailure.toLowerCase().equals("success")) {
            error.append("Error code: ").append(response.get("L_ERRORCODE0")).append("\n");
            error.append("Short message: ").append(response.get("L_SHORTMESSAGE0")).append("\n");
            error.append("Long message: ").append(response.get("L_LONGMESSAGE0")).append("\n");
            error.append("Please contact Laser Commando for bookings");
            throw new Exception(error.toString());
        }
    }

}
