/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.MapDao;
import au.com.lasercommando.model.map.Map;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author KobusVM
 */
@Service
public class MapService implements java.io.Serializable {
    @Resource
    private MapDao mapDao;
        
    public List<Map> findAll() {
        return mapDao.findAll();
    }
    
    public Map findOne(String id) {
        return mapDao.findOne(id);
    }

    public Map save(Map map) {
        Map savedMap = mapDao.save(map);
        return savedMap;
    }
}
