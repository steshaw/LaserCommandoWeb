/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.Constants;
import au.com.lasercommando.model.event.Contact;
import au.com.lasercommando.model.event.Event;
import au.com.lasercommando.model.event.Payment;
import au.com.lasercommando.model.event.SessionEnum;
import au.com.lasercommando.model.map.Map;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class MailService implements java.io.Serializable {

    private static Logger logger = Logger.getLogger(MailService.class);
    @Value("${mail.from}")
    private String mailFrom;
    @Value("${join.game.url}")
    private String joinGameUrl;
    @Value("${server.address}")
    private String serverAddress;
    @Resource
    private JavaMailSenderImpl mailSender;
    @Resource(name = "taskExecutorMail")
    private TaskExecutor taskExecutor;

    public void sendLocationBillReport(Map map, int totalPlayerCount, double pricePerPlayer, double totalAmount, Date startDate, Date endDate) {
        String htmlMessage = loadHtmlFile("/resources/locationBillReport.html");
        htmlMessage = populateDateInformation(htmlMessage, startDate, endDate);
        htmlMessage = populateLocationInformation(htmlMessage, map);
        htmlMessage = populateBillInformation(htmlMessage, totalPlayerCount, pricePerPlayer, totalAmount);

        List<Contact> contactList = map.getContactList();
        if (contactList != null) {
            for (Contact contact : contactList) {
                String mailTo = contact.getEmail();
                sendMail("Laser Commando - Location bill report", htmlMessage, mailTo);

            }
        }
    }

    public void sendBookingClosedMail(Event event, Payment payment) {
        String htmlMessage = loadHtmlFile("/resources/bookingClosed.html");
        htmlMessage = populateEventInformation(htmlMessage, event, payment);
        htmlMessage = populateLocationInformation(htmlMessage, event);

        List<Contact> contactList = event.getMap().getContactList();
        if (contactList != null) {
            for (Contact contact : contactList) {
                String mailTo = contact.getEmail();
                sendMail("Laser Commando - Booking closed", htmlMessage, mailTo);
            }
        }
    }

    public void sendLocationBookingMail(Event event, Payment payment) {
        String htmlMessage = loadHtmlFile("/resources/locationBooking.html");
        htmlMessage = populateEventInformation(htmlMessage, event, payment);
        htmlMessage = populateLocationInformation(htmlMessage, event);

        List<Contact> contactList = event.getMap().getContactList();
        if (contactList != null) {
            for (Contact contact : contactList) {
                String mailTo = contact.getEmail();
                sendMail("Laser Commando - Booking location for a new game", htmlMessage, mailTo);
            }
        }
    }

    public void sendJoiningMail(Event event, Payment payment) {
        String htmlMessage = loadHtmlFile("/resources/joiningEventConfirmation.html");
        htmlMessage = populateEventInformation(htmlMessage, event, payment);
        htmlMessage = populateContactInformation(htmlMessage, payment);
        htmlMessage = populateLocationInformation(htmlMessage, event);

        String mailTo = payment.getContact().getEmail();
        sendMail("Laser Commando - Joining an existing game confirmation", htmlMessage, mailTo);
    }

    public void sendNewInvitationMail(Event event, Payment payment) {
        String htmlMessage = loadHtmlFile("/resources/invitation.html");

        htmlMessage = populateEventInformation(htmlMessage, event, payment);
        htmlMessage = populateContactInformation(htmlMessage, payment);
        htmlMessage = populateLocationInformation(htmlMessage, event);
        htmlMessage = populateJoinUrl(htmlMessage, event);

        String attachment = "/forms/LaserCommandoMembershipForm.pdf";
        String formName = "LaserCommandoMembershipForm.pdf";
        String mailTo = payment.getContact().getEmail();
        sendMail("Laser Commando - Invitation", htmlMessage, mailTo, formName, attachment);
    }

    public void sendNewBookingMail(Event event, Payment payment) {
        String htmlMessage = loadHtmlFile("/resources/bookingConfirmation.html");
        htmlMessage = populateEventInformation(htmlMessage, event, payment);
        htmlMessage = populateContactInformation(htmlMessage, payment);
        htmlMessage = populateLocationInformation(htmlMessage, event);
        htmlMessage = populateInvoiceInformation(htmlMessage, event, payment);

        String mailTo = payment.getContact().getEmail();
        sendMail("Laser Commando - Creating a new game confirmation", htmlMessage, mailTo);
    }

    private void sendMail(String subject, String htmlMessage, String mailTo) {
        taskExecutor.execute(new NotificationTask(mailFrom, mailTo, subject, htmlMessage));
    }

    private void sendMail(String subject, String htmlMessage, String mailTo, String attachmentName, String attachment) {
        taskExecutor.execute(new NotificationTask(mailFrom, mailTo, subject, htmlMessage, attachmentName, attachment));
    }

    private String loadHtmlFile(String fileLocation) {
        StringBuilder html = new StringBuilder();
        try {
            URL htmlPage = new URL(serverAddress + fileLocation);
            BufferedReader in = new BufferedReader(new InputStreamReader(htmlPage.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                html.append(inputLine);
            }
            in.close();

        } catch (Exception ex) {
            logger.error("Reading html page failed " + fileLocation, ex);
        }
        return html.toString();
    }

    private String populateEventInformation(String html, Event event, Payment payment) {
        String sessionDescription = SessionEnum.findEnum(event.getSession()).getDescription();

        String newHtml = html.replace("{event.name}", event.getName()).replace("{event.privateEvent}", Boolean.toString(event.isPrivateEvent())).replace("{event.maxPlayerCount}", String.valueOf(event.getMaxPlayerCount())).replace("{payment.paidForPlayers}", String.valueOf(payment .getPaidForPlayers())).replace("{event.session}", sessionDescription).replace("{payment.totalCost}", String.valueOf(payment.getTotalCost())).replace("{payment.totalDiscount}", String.valueOf(payment.getDiscountAmount())).replace("{event.date}", event.getDateFormatted());

        return newHtml;
    }

    private String populateInvoiceInformation(String html, Event event, Payment payment) {
        Date today = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
        String todayFormatted = dateFormat.format(today);

        String newHtml = html.replace("{payment.id}", payment.getId()).replace("{invoice.date}", todayFormatted);

        return newHtml;
    }

    private String populateContactInformation(String html, Payment payment) {
        if (payment.getContact().getGroupName() == null) {
            payment.getContact().setGroupName("");
        }
                
        String newHtml = html.replace("{payment.contact.firstname}", payment.getContact().getFirstname());
        newHtml = newHtml.replace("{payment.contact.surname}", payment.getContact().getSurname());
        newHtml = newHtml.replace("{payment.contact.telephoneNumber}", payment.getContact().getTelephoneNumber());
        newHtml = newHtml.replace("{payment.contact.email}", payment.getContact().getEmail());
        newHtml = newHtml.replace("{payment.contact.groupName}", payment.getContact().getGroupName());
        newHtml = newHtml.replace("{payment.contact.averageAge}", payment.getContact().getAverageAgeGroupDescription());

        return newHtml;
    }

    private String populateLocationInformation(String html, Event event) {
        String newHtml = html.replace("{event.map.name}", event.getMap().getName()).replace("{event.map.address1}", event.getMap().getAddress1()).replace("{event.map.address2}", event.getMap().getAddress2()).replace("{event.map.city}", event.getMap().getCity()).replace("{event.map.postalCode}", event.getMap().getPostalCode()).replace("{event.map.state}", event.getMap().getState()).replace("{event.map.description}", event.getMap().getDescription());

        return newHtml;
    }

    private String populateLocationInformation(String html, Map map) {
        String newHtml = html.replace("{map.name}", map.getName()).replace("{map.address1}", map.getAddress1()).replace("{map.address2}", map.getAddress2()).replace("{map.city}", map.getCity()).replace("{map.postalCode}", map.getPostalCode()).replace("{map.state}", map.getState()).replace("{map.description}", map.getDescription());

        return newHtml;
    }

    private String populateDateInformation(String html, Date startDate, Date endDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);

        String newHtml = html.replace("{start.date}", simpleDateFormat.format(startDate)).replace("{end.date}", simpleDateFormat.format(endDate));

        return newHtml;
    }

    private String populateBillInformation(String html, int totalPlayerCount, double pricePerPlayer, double totalAmount) {
        String newHtml = html.replace("{total.player.count}", String.valueOf(totalPlayerCount)).replace("{price.per.player}", String.valueOf(pricePerPlayer)).replace("{total.cost}", String.valueOf(totalAmount));

        return newHtml;
    }

    private String populateJoinUrl(String html, Event event) {
        if (!joinGameUrl.endsWith("/")) {
            joinGameUrl = joinGameUrl + "/";
        }

        joinGameUrl = joinGameUrl + event.getId();
        String newHtml = html.replace("{join.game.url}", joinGameUrl);

        return newHtml;
    }

    private final class NotificationTask implements Runnable {

        private String mailFrom;
        private String mailTo;
        private String subject;
        private String htmlMessage;
        private String attachmentName;
        private String attachment;

        public NotificationTask(String mailFrom, String mailTo, String subject, String htmlMessage) {
            this.mailFrom = mailFrom;
            this.mailTo = mailTo;
            this.subject = subject;
            this.htmlMessage = htmlMessage;
        }

        public NotificationTask(String mailFrom, String mailTo, String subject, String htmlMessage, String attachmentName, String attachment) {
            this.mailFrom = mailFrom;
            this.mailTo = mailTo;
            this.subject = subject;
            this.htmlMessage = htmlMessage;
            this.attachmentName = attachmentName;
            this.attachment = attachment;
        }

        @Override
        public void run() {
            MimeMessage message = mailSender.createMimeMessage();
            try {
                MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(message, true);
                if (mailTo != null) {
                    mimeMessageHelper.setFrom(mailFrom);
                    mimeMessageHelper.setTo(mailTo);
                    mimeMessageHelper.setSubject(subject);
                    mimeMessageHelper.setText(htmlMessage, true);

                    if (attachment != null) {
                        InputStreamSource inputStreamSource = new InputStreamSource() {

                            @Override
                            public InputStream getInputStream() throws IOException {
                                URL attachmentUrl = new URL(serverAddress + attachment);
                                return attachmentUrl.openStream();
                            }
                        };
                        mimeMessageHelper.addAttachment(attachmentName, inputStreamSource);
                    }

                    mailSender.send(message);
                }
            } catch (MessagingException ex) {
                logger.error("The following error occured while trying to send a mail", ex);
            }
        }
    }
}
