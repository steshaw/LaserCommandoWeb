/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.AlbumDao;
import au.com.lasercommando.model.gallery.Album;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class AlbumService {

    @Resource
    private AlbumDao albumDao;

    public List<Album> findAll() {
        return albumDao.findAll();
    }

    public void saveAll(List<Album> albumList) {
        albumDao.save(albumList);
    }

    public void deleteAll() {
        albumDao.deleteAll();
    }
}
