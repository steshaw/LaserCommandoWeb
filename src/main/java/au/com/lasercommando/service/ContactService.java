/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.ContactDao;
import au.com.lasercommando.model.event.Contact;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class ContactService implements java.io.Serializable {
    @Resource
    private ContactDao contactDao;

    public Contact findOne(String id) {
        return contactDao.findOne(id);
    }

    public Contact save(Contact contact) {
        Contact savedContact = contactDao.save(contact);
        return savedContact;
    }
}
