/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.dao.PostDao;
import com.restfb.types.Post;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class PostService implements java.io.Serializable {
    @Resource
    private PostDao postDao;

    public Post findOne(String id) {
        return postDao.findOne(id);
    }
    
    public Post findFirst() {
        Post firstPost = new Post();
        List<Post> postList = postDao.findAll();
        if ((postList != null) && (postList.size() > 0)) {
            firstPost = postList.get(0);
        }
        
        return firstPost;
    }

    public Post save(Post post) {
        Post firstPost = findFirst();
        Post savedPost = firstPost;       
        if (!post.getMessage().equals(firstPost.getMessage())) {
            //add the newest post to the database and deletet the previous post
            postDao.deleteAll();
            savedPost = postDao.save(post);            
        } 
        
        return savedPost;
    }
}
