/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.model.event.EventProxy;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author dutoitk
 */
public class EventsSorter implements Comparator<EventProxy> {

    @Override
    public int compare(EventProxy o1, EventProxy o2) {
        Date o1Date = o1.getEvent().getDate();
        Date o2Date = o2.getEvent().getDate();
        int result = o1Date.compareTo(o2Date);

        if (result != 0) {
            return result;
        } else {
            result = new Integer(o1.getEvent().getSession()).compareTo(new Integer(o2.getEvent().getSession()));
            return result;
        }
    }

}
