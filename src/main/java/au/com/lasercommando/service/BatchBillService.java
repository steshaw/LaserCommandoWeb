/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.model.event.EventProxy;
import au.com.lasercommando.model.event.Payment;
import au.com.lasercommando.model.map.AuthorEnum;
import au.com.lasercommando.model.map.Map;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author KobusVM
 */
@Service
public class BatchBillService implements java.io.Serializable  {
    private static Logger logger = Logger.getLogger(BatchBillService.class);
    @Resource
    private EventService eventService;
    @Resource
    private MapService mapService;
    @Resource
    private MailService mailService;
    private boolean testBillingReport = true;
    
    public void execute() {
        //test that the batch only runs on a Sunday
        
        //today will be Sunday
        Date endDate = new Date();
        if (testBillingReport) {
            GregorianCalendar endDateCal = new GregorianCalendar(2012, 6, 22);
            endDate = endDateCal.getTime();
        }
        
        //startdate will be Monday
        GregorianCalendar startDateCal = new GregorianCalendar();
        startDateCal.setTime(endDate);
        startDateCal.add(Calendar.DAY_OF_MONTH, -6);
        Date startDate = startDateCal.getTime();

        logger.info("Find all maps where Laser Commando plays");
        List<Map> mapList = mapService.findAll();
        for (Map map : mapList) {
            int paidForPlayers = 0;
            //get all the events held at this map
            logger.info("Find all events for map " + map.getName());
            List<EventProxy> eventList = eventService.findEvents(AuthorEnum.LASER_COMMANDO.getCode(), startDate, endDate, map.getName());            
            for (EventProxy eventProxy : eventList) {
                //get all the payments made per event
                logger.info("Find all payments for event " + eventProxy.getEvent().getName());
                List<Payment> paymentList = eventProxy.getEvent().getPaymentList();
                for (Payment payment : paymentList) {
                    //only count payments that went through paypal process
                    if (payment.isPaid()) {
                        logger.info("Players paid for " + payment.getPaidForPlayers() + " at the map " + map.getName());
                        paidForPlayers = paidForPlayers + payment.getPaidForPlayers();
                    }
                }
            }

            if (paidForPlayers > 0) {
                double totalAmount = paidForPlayers * map.getPricePerPlayer();
                logger.info("Send bill report for map " + map.getName() + " startDate: " + startDate + " endDate: " + endDate + " totalAmount: " + totalAmount + " paidForPlayers: " + paidForPlayers + " PricePerPlayer: " + map.getPricePerPlayer());
                mailService.sendLocationBillReport(map, paidForPlayers, map.getPricePerPlayer(), totalAmount, startDate, endDate);
            }
        }
    }
}
