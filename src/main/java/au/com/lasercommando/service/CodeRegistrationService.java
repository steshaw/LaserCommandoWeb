/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando.service;

import au.com.lasercommando.model.event.EventProxy;
import au.com.lasercommando.model.player.*;
import java.util.List;
import java.util.Locale;
import javax.annotation.Resource;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * @author dutoitk
 */
@Service
public class CodeRegistrationService implements java.io.Serializable {

    @Resource
    private MessageSource messageSource;
    @Resource
    private PlayerService playerService;
    @Resource
    private EventService eventService;
    @Resource
    private GenAchievementService genAchievementService;

    public void registerGameCode(Player player, String gameCode) throws Exception {
        List<String> gameCodeList = player.getGameCodeList();
        for (String listItemValue : gameCodeList) {
            if (listItemValue.equals(gameCode)) {
                String error = messageSource.getMessage("game.code.registration.error", new Object[]{}, Locale.US);
                throw new Exception(error);
            }
        }

        EventProxy eventProxy = eventService.findEventByAlternateId(gameCode);
        if (eventProxy == null) {
            String error = messageSource.getMessage("game.code.invalid", new Object[]{}, Locale.US);
            throw new Exception(error);
        } else {
            //add game code to player
            player.addGameCode(gameCode);

            //increment games played
            int gamesPlayed = player.getGamesPlayed();
            gamesPlayed = gamesPlayed + 1;
            player.setGamesPlayed(gamesPlayed);

            //determine if the rank should be incremented
            RankEnum[] rankEnumValues = RankEnum.values();
            int length = rankEnumValues.length;
            for (int i = 0; i < length; i++) {
                if (gamesPlayed == rankEnumValues[i].getGamesRequiredToNextLevel()) {
                    int currentRank = player.getRank();
                    player.setRank(currentRank + 1);
                    break;
                }
            }
            playerService.save(player);
        }
    }

    public void registerAchievementCode(Player player, String achievementCode) throws Exception {
        List<String> achievementCodeList = player.getAchievementCodeList();
        for (String listItemValue : achievementCodeList) {
            if (listItemValue.equals(achievementCode)) {
                String error = messageSource.getMessage("achievement.code.registration.error", new Object[]{}, Locale.US);
                throw new Exception(error);
            }
        }

        GenAchievement genAchievement = genAchievementService.findGenAchievementByAlternateId(achievementCode);
        if (genAchievement == null) {
            String error = messageSource.getMessage("achievement.code.invalid", new Object[]{}, Locale.US);
            throw new Exception(error);
        } else if (genAchievement.getTotalAvailable() == genAchievement.getTotalUsed()) {
            String error = messageSource.getMessage("achievement.code.total.reached", new Object[]{}, Locale.US);
            throw new Exception(error);
        } else {
            //find the achievement in the player's list
            boolean achievementInList = false;
            List<Achievement> achievementList = player.getAchievementList();
            for (Achievement achievement : achievementList) {
                if (achievement.getAchievementCode() == genAchievement.getAchievementCode()) {
                    if (achievement.getNumberDone() == achievement.getNumberRequired()) {
                        //if end reached of achievement, don't do anything
                    } else {
                        //achievement found so increment number
                        achievement.setNumberDone(achievement.getNumberDone() + 1);
                    }
                    achievementInList = true;
                    break;
                }
            }

            if (!achievementInList) {
                AchievementEnum achievementEnum = AchievementEnum.findEnum(genAchievement.getAchievementCode());
                if (achievementEnum.isActive()) {
                    //achievement must be new, add it to the player
                    Achievement achievement = new Achievement();
                    achievement.setAchievementCode(genAchievement.getAchievementCode());
                    achievement.setDescription(achievementEnum.getDescription());
                    achievement.setName(achievementEnum.getName());
                    achievement.setNumberDone(1);
                    achievement.setNumberRequired(achievementEnum.getNumberRequired());
                    player.addAchievement(achievement);
                }
            }
        }
        //also add the code used so that the player can't use the code again
        player.getAchievementCodeList().add(achievementCode);
        playerService.save(player);

        //increment generated achievement used
        genAchievement.setTotalUsed(genAchievement.getTotalUsed() + 1);
        genAchievementService.save(genAchievement);
    }
}
