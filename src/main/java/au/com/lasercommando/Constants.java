/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.lasercommando;

/**
 *
 * @author dutoitk
 */
public interface Constants {
    public static final String DATE_FORMAT = "E dd/MM/yyyy";
    public static final String DATE_FORMAT_JUST_NUMBERS = "dd/MM/yyyy";
    public static final String TIME_ZONE = "GMT+10";
    public static final int MAX_PLAYERS = 40;
    public static final int MIN_PLAYERS = 10;
    public static final int CAROUSEL_AUTO_PLAY_INTERVAL = 8000;
    public static final int CAROUSEL_AUTO_PLAY_INTERVAL_LONG = 16000;
    public static final int CAROUSEL_EFFECT_DURATON = 1000;
    public static final String DECIMAL_FORMAT = "#0.00";
}
