/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nvp.codegenerator;

import au.com.lasercommando.model.product.ProductOrder;
import com.paypal.sdk.core.nvp.NVPDecoder;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author KobusVM
 * 
 * After test go to one of the following URL's
 * 
 * https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=EC-37R19029RV807820D
 * https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=EC-12154232SV033011J&useraction=commit
 */
public class ECSetExpressCheckoutTest extends TestCase {
    
    public ECSetExpressCheckoutTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    // TODO add test methods here. The name must begin with 'test'. For example:
    public void testExecute() {
        List<ProductOrder> productOrderList = new ArrayList<ProductOrder>();
        ECSetExpressCheckout ecSetExpressCheckout = new ECSetExpressCheckout();
        NVPDecoder response = ecSetExpressCheckout.ECSetExpressCheckoutCode("http://localhost:8084/LaserCommando/payPalReturn?eventId=123",
                "http://localhost/cancel", "0", "30", "Sale", "AUD", "kobusd_1341377572_biz_api1.gmail.com",
                "1341377591", "AWmIFWJIxmVfpHQc4KbGAoVoaSInA82M8xsIUdZPsBH-BWlbeqcuLfA2", "sandbox",
                "30", "Laser Commando Booking", "1", "89.0", productOrderList);
        System.out.println(response.get("ACK"));
    }
}
